import { Component } from '@angular/core';

//declare var x3dom: any;
//declare var defineClass: any;

@Component({
    selector: 'my-app',
    template: `
        <x3dom></x3dom>
        <sidebar></sidebar>
    `,
    styles: [`
        .body {
            background-color: lightblue;
        }    
    `]
})
export class MapViewComponent {

    constructor() {
    }

}