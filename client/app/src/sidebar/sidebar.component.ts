import {Component} from '@angular/core';
import {BvhRefinerSettingsComponent} from './bvhRefinerSettings/bvhRefinerSettings.component';
import { ModalService } from '../modal/modal.service';

@Component({
    selector: 'sidebar',
    templateUrl: './sidebar.template.html',
    styles: [`
        .sidebar {
            position: absolute;
            left: 0;
            top: 0;
            bottom: 0;
            width: 300px;
            border-right: 1px solid #C3C3C3;
            background-color: #F0F0F0;
        }

        .sidebar_toggler {
            color: black;
            font-size: 50px;
            position: fixed;
            top: 5px;
            right: 5px;
            width: 50px;
            height: 50px;
            z-index: 1000;
        }

        .sidebar_toggler > span {
            display: block;
            background-color: #B2B2B2;
            height: 10px;
            width: 100%;
            margin-top: 5px;
            margin-bottom: 5px;
        }
    `]
})
export class SidebarComponent {
    
    settings;
    show:boolean = false;
    
    constructor(private modalService:ModalService) {

        window.addEventListener('keypress', (e) => {
            if(e.charCode == 112) {
                this.toggle();
            }
        });
    }

    toggleIt(e) {
        console.log(e);
        this.modalService
            .setHeader("test")
            .setContent("Test content")
            .setShowBasicControllers(true)
            .show();
    }

    toggle() {
        this.show = !this.show;
    }
}