import {Component, ChangeDetectorRef} from '@angular/core';

import {BvhRefinerSettingsService} from '../../x3d_components/bvhRefiner/bvhRefinerSettings.service';

@Component({
    selector: 'bvhRefinerSettings',
    templateUrl: 'bvhRefinerSettings.template.html',
    styles: [`
        .fa {
            color: #CCCCCC;
            cursor: pointer;
        }
        
        .settings-element-content {
            overflow: hidden;
        }
    `]
})
export class BvhRefinerSettingsComponent {
    
    bvhRefinerSettings;
    settings;
    maximized = false;
    
    constructor(bvhRefinerSettings: BvhRefinerSettingsService, private cd: ChangeDetectorRef) {
        this.bvhRefinerSettings = bvhRefinerSettings;
        this.settings = this.bvhRefinerSettings.getSettings();

        this.bvhRefinerSettings.getUpdateHandle().subscribe((settings) => {
            this.settings = settings;
            this.cd.markForCheck();
        })
    }

    change($event) {
        this.bvhRefinerSettings.updateSettings(this.settings);
    }

    toggle() {
        this.maximized = !this.maximized;
    }
    
}