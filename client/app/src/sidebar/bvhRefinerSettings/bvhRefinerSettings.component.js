System.register(['@angular/core', '../../x3d_components/bvhRefiner/bvhRefinerSettings.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, bvhRefinerSettings_service_1;
    var BvhRefinerSettingsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (bvhRefinerSettings_service_1_1) {
                bvhRefinerSettings_service_1 = bvhRefinerSettings_service_1_1;
            }],
        execute: function() {
            BvhRefinerSettingsComponent = (function () {
                function BvhRefinerSettingsComponent(bvhRefinerSettings, cd) {
                    var _this = this;
                    this.cd = cd;
                    this.maximized = false;
                    this.bvhRefinerSettings = bvhRefinerSettings;
                    this.settings = this.bvhRefinerSettings.getSettings();
                    this.bvhRefinerSettings.getUpdateHandle().subscribe(function (settings) {
                        _this.settings = settings;
                        _this.cd.markForCheck();
                    });
                }
                BvhRefinerSettingsComponent.prototype.change = function ($event) {
                    this.bvhRefinerSettings.updateSettings(this.settings);
                };
                BvhRefinerSettingsComponent.prototype.toggle = function () {
                    this.maximized = !this.maximized;
                };
                BvhRefinerSettingsComponent = __decorate([
                    core_1.Component({
                        selector: 'bvhRefinerSettings',
                        templateUrl: 'app/src/sidebar/bvhRefinerSettings/bvhRefinerSettings.template.html',
                        styles: ["\n        .glyphicon {\n            color: #CCCCCC;\n            cursor: pointer;\n        }\n    "]
                    }), 
                    __metadata('design:paramtypes', [bvhRefinerSettings_service_1.BvhRefinerSettingsService, core_1.ChangeDetectorRef])
                ], BvhRefinerSettingsComponent);
                return BvhRefinerSettingsComponent;
            }());
            exports_1("BvhRefinerSettingsComponent", BvhRefinerSettingsComponent);
        }
    }
});
//# sourceMappingURL=bvhRefinerSettings.component.js.map