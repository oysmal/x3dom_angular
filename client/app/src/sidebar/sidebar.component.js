System.register(['angular2/core', './bvhRefinerSettings/bvhRefinerSettings.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, bvhRefinerSettings_component_1;
    var SidebarComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (bvhRefinerSettings_component_1_1) {
                bvhRefinerSettings_component_1 = bvhRefinerSettings_component_1_1;
            }],
        execute: function() {
            SidebarComponent = (function () {
                function SidebarComponent() {
                    var _this = this;
                    window.addEventListener('keypress', function (e) {
                        if (e.key == "p") {
                            _this.show = !_this.show;
                        }
                    });
                }
                SidebarComponent = __decorate([
                    core_1.Component({
                        selector: 'sidebar',
                        templateUrl: 'app/src/sidebar/sidebar.template.html',
                        styles: ["\n        .sidebar {\n            position: absolute;\n            left: 0;\n            top: 1em;\n            bottom: 1em;\n            width: 300px;\n\n            box-shadow: 5px 5px 5px #111111;\n            background-color: #F0F0F0;\n        }\n    "],
                        directives: [bvhRefinerSettings_component_1.BvhRefinerSettingsComponent]
                    }), 
                    __metadata('design:paramtypes', [])
                ], SidebarComponent);
                return SidebarComponent;
            }());
            exports_1("SidebarComponent", SidebarComponent);
        }
    }
});
//# sourceMappingURL=sidebar.component.js.map