import { Component, AfterViewInit } from '@angular/core';
import { ModalService } from '../modal/modal.service';
import { SliceService } from '../slices/slice.service';
import { Slice } from '../slices/slice';

declare var $:any;
declare var x3dom:any;
declare var window:any;

@Component({
    selector: '[slicejs]',
    template: `
        <transform id='slicejs'></transform>
    `
})
export class SliceJSComponent implements AfterViewInit {

    ref:any;
    substituteImageUrl:string = '/resources/red_opaque_500x500.png';
    radius:number = 1;
    slices:Array<Slice>;
    viewpointDistance = 30;
    
    constructor(private sliceService:SliceService, private modalService:ModalService) {
    }

    ngAfterViewInit() {
        this.ref = $('#slicejs');

        let x = this.sliceService.slices.subscribe(slices => {
            this.slices = slices;
            this.setupView();
            x.unsubscribe();
        });

        this.sliceService.sliceChanged.subscribe((slice:Slice) => {
            this.onChangeSingleSlice(slice);
        });

        window.onClickSlice = (e, slice_index) => {
            this.onClickedSlice(e, this.slices[slice_index]);
        }
    }

    setupView() {
        let slices = "";

        // create slices html
        this.slices.map(s => {
            if(s.visible) slices += this.getSliceTemplate(s);
            else slices += this.getSubsituteTemplate(s);
        });
        
        this.ref.html(slices); // load the slices into ref
    }

    getSubsituteTemplate(slice):string {        
        return `
             <transform 
                id="${slice.id+'_outer'}"
                translation="${slice.translation}"
                rotation="${slice.rotation}">

                    <shape id="${slice.id+'_slice'}" onclick="onClickSlice(event, ${slice.index.toFixed()})">
                        <appearance>
                            <ImageTexture url="${this.substituteImageUrl}"></ImageTexture>
                        
                        </appearance>
                        <sphere id="tester" radius="${this.radius}"></sphere>
                    </shape>

                    <Viewpoint id="${slice.id+'_viewpoint'}" position="0 -150 0" orientation="-30 1 0 -1.57" set_bind="false"></Viewpoint>
            </transform>
        `;
    }

    getSliceTemplate(slice):string {
        return `
            <transform 
                id="${slice.id+'_outer'}"
                translation="${slice.translation}"
                rotation="${slice.rotation}">

                    <shape id="${slice.id+'_slice'}" onclick="onClickSlice(event, ${slice.index.toFixed()})">
                        <appearance>
                            <ImageTexture url="${slice.textureUrl}"></ImageTexture>
                        
                        </appearance>
                        <box size="${slice.size}"></box>
                    </shape>

                    <Viewpoint 
                        id="${slice.id+'_viewpoint'}"
                        position="0 -150 0"
                        orientation="-30 1 0 -1.57"
                        set_bind="false">
                    </Viewpoint>
            </transform>
        `;
    }

    /** 
     * Performs all required actions to refresh the slice visualization when a slice is changed.
     * @param slice the Slice that has been changed.
     */
    onChangeSingleSlice(slice:Slice) {
        this.slices[slice.index.toFixed()] = slice; // update our array

        $('#'+slice.id+'_outer').remove(); // remove the changed slice from the dom

        // Get the updated html template view for the slice and append it to the slicejs element
        let new_html_template = slice.visible ? this.getSliceTemplate(slice) : this.getSubsituteTemplate(slice);
        this.ref.append(new_html_template);
        x3dom.reload();
    }

    onClickedSlice(click_event, slice) {

        // If the left mouse button was clicked, toggle the visibility and update the sliceService
        // Else (right button) open the modal information window for the slice. 
        if(click_event.button == 1) {
            slice.visible = !slice.visible;
            this.sliceService.changeSlice(slice);
        } else {
            this.modalService
                .reset()
                .setSlice(slice)
                .setShowBasicControllers(false)
                .show();
        }
    }
}

