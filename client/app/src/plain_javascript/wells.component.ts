import { Component, AfterViewInit } from '@angular/core';
import { ModalService } from '../modal/modal.service';
import { WellService } from '../x3d_components/well/well.service';
import { ViewpointService } from '../x3d_components/viewpoints/viewpoint.service';
import { Well } from '../x3d_components/well/well';

declare var $:any;
declare var x3dom:any;

@Component({
    selector: '[welljs]',
    template: `
        <transform id='welljs'></transform>
    `
})
export class WellJSComponent implements AfterViewInit {
    private ref:any;
    private wells:Array<Well>;
    private canvas:any;
    private ctx:any;
    private well_radius = 1;
    private bg_image_url = "/resources/red_opaque_500x500.png";

    constructor(private wellService:WellService, private modalService:ModalService) {
        // canvas setup
        this.canvas = document.createElement('canvas');     // create canvas element
        this.ctx = this.canvas.getContext('2d');                 // get context
        let gr = this.ctx.createLinearGradient(0, 0, 101, 0);   // create a gradient
        let i = 0, cs;

        this.canvas.width = 101;                                // 101 pixels incl.
        this.canvas.height = 1;                                 // as the gradient
        gr.addColorStop(0.0, '#7700ff');
        gr.addColorStop(0.25, '#0000ff');
        gr.addColorStop(0.5, '#00ff00');
        gr.addColorStop(0.75, '#ffff00');
        gr.addColorStop(1.0, '#FF0000');

        this.ctx.fillStyle = gr;                                // set as fill style
        this.ctx.fillRect(0, 0, 101, 1);
    }

    ngAfterViewInit() {
        this.ref = $('#welljs');

        let sub = this.wellService.getSubscriptionHandle().subscribe((wells:Array<Well>) => {
            this.wells = wells;
            this.setupView();
            sub.unsubscribe();
        });

        this.wellService.getChangeSingleWellHandle().subscribe( (well) => {
            this.onChangeSingleWell(well);
        });
    }

    setupView() {
        let wells_html = "";
        let funcs = [];

        // create slices html
        this.wells.map(w => {
            let well_listeners;
            if(w.visible) {
                let wells_view = this.getWellTemplate(w);
                wells_html += wells_view[0];
                well_listeners = wells_view[1];
            }
            else {
                let wells_view = this.getSubstituteTemplate(w);
                wells_html += wells_view[0];
                well_listeners = wells_view[1];
            }
            
            funcs.push(() => {
                well_listeners.map(x => {
                    x();
                });
            });
        });
        
        this.ref.html(wells_html); // load the wells into ref
        // when this has loaded, execute the functions to add event listeners
        this.ref.ready(() => {
            funcs.map(x => {
                x();
            });
        });
    }
    
     /** 
     * Performs all required actions to refresh the well visualization when a well is changed.
     * @param well the Well that has been changed.
     */
    onChangeSingleWell(well:Well) {
        this.wells[well.index.toFixed()] = well; // update our array

        $('#'+well.id+'_well').remove(); // remove the changed well from the dom

        // Get the updated html template view for the well and append it to the welljs element
        let new_view:any = well.visible ? this.getWellTemplate(well) : this.getSubstituteTemplate(well);
        this.ref.append(new_view[0]);
        
        // setup click listener
        this.ref.ready(() => {
            new_view[1].map(x => {
                x();
            })
        });
    }

    getWellTemplate(well:Well) {
        let wellVisualizationObject = this.createWellVisualizationObject(well);
        let cylinders = "";
        let rotation = [1, 0, 0, Math.PI/2]; // 90 degree rotation around x-axis to get vertical position

        let listeners = [];
        wellVisualizationObject.positions.map((pos, i) => {
            cylinders += `
                <transform rotation="${rotation}" translation="${pos}">
                    <shape id="${well.id+'_well_cylinder_'+i}">
                        <appearance>
                            <material diffuseColor="${wellVisualizationObject.colors[i]}"></material>
                        </appearance>
                        <cylinder radius="${this.well_radius}" height="${wellVisualizationObject.depths[i]}"></cylinder>
                    </shape>
                </transform>
            `;

            listeners.push(() => {
                $(`#${well.id}_well_cylinder_${i}`).on('click', (e) => {
                    e.clickValue = well.propertyValues[well.activeVisualizationIndex + i*well.property_values_row_length];
                    this.onClickWell(e, well)
                });
            });
        });
        return [`
                <transform id="${well.id+'_well'}">
                    ${cylinders}
                </transform>
                `, listeners];
    }

    getSubstituteTemplate(well:Well) {
        let listeners = [
            () => {
                $(`#${well.id}_well_substitute`).on('click', (e) => {
                    this.onClickWell(e, well);
                })
            }
        ];

        let position = `${well.positions[0]},${well.positions[1]},${well.positions[2]}`;
        return [`
            <transform translation="${position}">
                <shape id="${well.id+'_well_substitute'}" >
                    <appearance>
                        <ImageTexture url="${this.bg_image_url}"></ImageTexture>
                    
                    </appearance>
                    <sphere radius="${this.well_radius}"></sphere>
                </shape>
            </transform>
        `, listeners ];
    }

    onClickWell(event, well) {

        if(event.button == 1 || !well.visible) {
            well.visible = !well.visible;
            this.wellService.changeSingleWell(well);
        } else {
            this.modalService
                .reset()
                .setShowBasicControllers(true)
                .setWell(well)
                .setValueClicked(event.clickValue)
                .show();
        }
    }

    createWellVisualizationObject(well:Well) {
        let wellVisualizationObject:any = {
            positions: [],
            depths: [],
            colors: []
        };

        let i_chronologically = 0;
        for(let i = 0; i < well.positions.length-3; i+=well.positions_row_length) {
            let pos = [well.positions[i], well.positions[i+1], well.positions[i+2]];

            let next_pos = [well.positions[i+well.positions_row_length], 
                            well.positions[i+well.positions_row_length+1], 
                            well.positions[i+well.positions_row_length+2]];

            let newpos = [pos[0]+(next_pos[0]-pos[0])/2, pos[1]+(next_pos[1]-pos[1])/2, -(next_pos[2]+pos[2])/2];
            wellVisualizationObject.positions.push(newpos.join(','));
            wellVisualizationObject.depths.push((next_pos[2]-pos[2]).toString());

            let propValue = well.propertyValues[well.activeVisualizationIndex + i_chronologically*well.property_values_row_length];
            let color = "0,0,0";
            if(propValue && propValue > 0) {
                let propertyValueRange = well.maxPropertyValue-well.minPropertyValue;
                let gradientPos = propValue/propertyValueRange*100;
                let tmp = this.ctx.getImageData(gradientPos, 0, 1, 1).data;
                color = [parseFloat(tmp[0])/255.0, parseFloat(tmp[1])/255.0, parseFloat(tmp[2])/255.0].join(',');
            }
            wellVisualizationObject.colors.push(color);
            i_chronologically++;
        }

        return wellVisualizationObject;
    }
}