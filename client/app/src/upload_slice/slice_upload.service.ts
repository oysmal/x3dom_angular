import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import { ApiConstants } from '../../../.tmp/apiConstants';
import { Observable }     from 'rxjs/Observable';

@Injectable()
export class SliceUploadService {

    private sliceResourceUrl = ApiConstants.api + "/slice_model";

    constructor(private http: Http) {

    }

    create_request(form_data) {
        let formData: FormData = new FormData();
        for(let key in form_data) {
            formData.append(key, form_data[key]);
        }
        return this.http.post(this.sliceResourceUrl, formData);
    }

    upload(body) {
        return this.http.post(this.sliceResourceUrl, body)
            .map((res: Response) => {
                let b = res.json();
                console.log(b);
                return b || {};
            }).catch((err: Response | any) => {
                console.log(err.json());
                return err.json();
            });
    }
}