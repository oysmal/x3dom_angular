import { Injectable } from '@angular/core';
import {AppSettings} from '../../settings/settings.service';

@Injectable()
export class CoordinateConverterService {

    constructor(private appSettings:AppSettings) {
    }

    getPositionInLocalCoordinateSystem(utmx:number, utmy:number) {
        console.log("x length: " + ((utmx - AppSettings.originUTMX)/AppSettings.lengthOfUnitX));
        console.log("y length: " + ((utmy - AppSettings.originUTMY)/AppSettings.lengthOfUnitY));
        return {
            x: (utmx - AppSettings.originUTMX)/AppSettings.lengthOfUnitX,
            y: (utmy - AppSettings.originUTMY)/AppSettings.lengthOfUnitY
        };
    }

    getDepthInLocalCoordinateSystem(depth:number) {
        return depth/AppSettings.lengthOfUnitX*50;
    }

    convertFromDMSToDecimal(deg:number, min:number, sec:number) {
        return deg+min/60+sec/3600;
    }
}