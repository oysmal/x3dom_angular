

export class AppSettings {

    static origin_latitude:number = 80.99738888888889;
    static origin_longitude:number = 8.228622222222223;
    static leftUTM:number = 381970.000;
    static upUTM:number = 8999810.000;
    static rightUTM:number = 850020.000;
    static downUTM:number = 8247060.000;
    static originUTMX:number = (AppSettings.leftUTM+AppSettings.rightUTM)/2;
    static originUTMY:number = (AppSettings.upUTM+AppSettings.downUTM)/2;
    static aspect:number = (AppSettings.upUTM-AppSettings.downUTM)/(AppSettings.rightUTM-AppSettings.leftUTM);

    static width:number = 1024;
    static height:number = 1024*AppSettings.aspect;

    static lengthOfUnitX:number = (AppSettings.rightUTM-AppSettings.leftUTM) / AppSettings.width;
    static lengthOfUnitY:number = (AppSettings.upUTM-AppSettings.downUTM) / AppSettings.height;

}
