import { Component, EventEmitter, Output, Input } from '@angular/core';

@Component({
    selector: 'multipleFileReader',
    template: `
        <div>
            Select file:
            <input type="file" multiple [disabled]="disabled" (change)="changeListener($event)">
        </div>
    `
})
export class FileReaderMultipleComponent {
    @Output('complete') complete = new EventEmitter();
    @Output('error') error = new EventEmitter();
    @Input('disabled') disabled = false;

    changeListener(event) {
        let textFiles = new Array(event.target.files.length);
        console.log(textFiles.length);
        let loadedTextFiles = new Array(event.target.files.length);
        for(let i = 0; i < event.target.files.length; i++) {
            let file = event.target.files[i];
            loadedTextFiles[i] = false;
            if (file) {
                console.log('file ' + i);
                var reader = new FileReader();
                reader.onload = (e:any) => {
                    let contents = e.target.result;
                    textFiles[i] = {
                        name: file.name,
                        contents: contents
                    };
                    loadedTextFiles[i] = true;
                    let alldone = loadedTextFiles[0];
                    for(let j = 1; j < loadedTextFiles.length; j++) {
                        alldone = alldone && loadedTextFiles[j];
                    }
                    console.log('alldone: ' + alldone);
                    if(alldone) {
                        this.complete.emit(textFiles);
                    } 
                }
                console.log('start reading');
                reader.readAsText(file);
            } else {
                this.error.emit("Failed to read file");
                break;
            }
        }
    }
}