import { Component, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'fileReader',
    template: `
        <div>
            Select file:
            <input type="file" (change)="changeListener($event)">
        </div>
    `
})
export class FileReaderComponent {
    @Output('complete') complete = new EventEmitter();
    @Output('error') error = new EventEmitter();

    changeListener(event) {
        let file = event.target.files[0];

        if (file) {
            var reader = new FileReader();
            reader.onload = (e:any) => {
                let contents = e.target.result;
                this.complete.emit(contents);
            }
            reader.readAsText(file);
        } else {
            this.error.emit("Failed to read file");
        }
    }
}