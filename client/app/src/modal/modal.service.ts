import { ReplaySubject } from 'rxjs';
import { ViewContainerRef } from '@angular/core';

export class ModalService {

    settings = {
        header: "",
        content: "",
        showBasicControllers: "",
        slice: null,
        well: null,
        valueClicked: null
    }
    visible: boolean = false;
    updates = new ReplaySubject(1);
    vcr:ViewContainerRef;

    reset() {
        this.settings.slice = null;
        this.settings.well = null;
        this.settings.valueClicked = null;
        return this;
    }

    show() {
        this.visible = true;
        console.log("SHOW MODAL");
        console.log(this.settings);
        this.updates.next(this.settings);
    }

    hide() {
        console.log("hide");
        this.visible = false;
        this.updates.next(null);
    }

    toggle() {
        this.visible = !this.visible;
        this.updates.next(this.visible? this.settings : null);
    }

    getUpdateHandle() {
        return this.updates;
    }

    setViewContainerRef(vcr) {
        this.vcr = vcr;
    }

    getViewContainerRef() {
        return this.vcr;
    }

    setHeader(header) {
        this.settings.header = header;
        return this;
    }

    setContent(content) {
        this.settings.content = content;
        return this;
    }

    setShowBasicControllers(showBasicControllers) {
        this.settings.showBasicControllers = showBasicControllers;
        return this;
    }

    setSlice(slice) {
        this.settings.slice = slice;
        return this;
    }

    setWell(well) {
        this.settings.well = well;
        return this;
    }

    setValueClicked(value) {
        this.settings.valueClicked = value;
        return this;
    }

    getHeader() {
        return this.settings.header;
    }

    getContent() {
        return this.settings.content;
    }

    getShowBasicControllers() {
        return this.settings.showBasicControllers;
    }

    getSlice() {
        return this.settings.slice;
    }

    getWell() {
        return this.settings.well;
    }

    getValueClicked() {
        return this.settings.valueClicked;
    }
}