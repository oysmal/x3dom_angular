import { Component, Input, Output, EventEmitter, OnInit, AfterViewInit, ChangeDetectorRef} from '@angular/core';
import { ModalService } from './modal.service';
import { WellService } from '../x3d_components/well/well.service';
import { Well } from '../x3d_components/well/well';

@Component({
    selector: 'wellModalContent',
    template: `
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-header">{{well.name}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" (click)="close($event)">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="description">
                    <h5>Description</h5>
                    <p>{{well.description}}</p>
                </div>
                <div class="related-articles" (click)="onClick($event)">
                    <h5 class="related-articles-header">Related Articles</h5>
                    <div class="related-articles-content">
                        <div class="list-group">
                            <a *ngFor="let item of well.related_links" [href]="item" class="list-group-item list-group-item-action">{{item}}</a>
                        </div>
                    </div>
                </div>
                <div class="visualization-ctrl">
                    <h5>Visualization options</h5>
                    
                    <div class="visualization-legend">
                        <h6>Legend</h6>
                        <span class="left-legend">{{minvalue}}</span>
                        <canvas id="canvas-legend"></canvas>
                        <span class="right-legend">{{maxvalue}}</span>

                        <div class="click-info">
                            <div *ngIf="valueClicked && valueClicked != -999"><span class="badge badge-default">Value at click point: {{valueClicked}}</span></div>
                        </div>
                    </div>

                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{well.propertyNames[well.activeVisualizationIndex]}}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a *ngFor="let propName of well.propertyNames; let i = index" class="dropdown-item" href="#" (click)="select_value_changed(i)">{{propName}}</a>
                        </div>
                    </div>

                    <div class="inputs">
                        <div class="input-group">
                            <span class="input-group-addon">Min Value</span>
                            <input class="form-control" type="number" [value]="minvalue" (input)="changeMinVal($event)">
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">Max Value</span>
                            <input class="form-control" type="number" [value]="maxvalue" (input)="changeMaxVal($event)">
                        </div>
                        <button class="btn btn-primary" (click)="updateVisualizationSettings($event)">Update Visualization</button>
                    </div>
                </div>
            </div>
            
            <div ng-if="showBasicControllers" class="modal-footer">
                <button (click)="close($event)" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    `,
    styles: [`

        h6 {
            font-size: 16px !important;
        }

        .left-legend {
            position: absolute;
            left: 15px;
        }

        .right-legend {
            position: absolute;
            right: 15px;
        }

        .visualization-legend {
            text-align: center;
            margin-bottom: 15px;
            margin-top: 15px;
        }

        .dropdown > button {
            width: 100%;
            margin-bottom: 10px;
        }

        span.badge-default {
            padding: 7px !important;
            font-weight: 500 !important;
            font-size: 14px !important;
            width: 80% !important;
        }
        canvas {
            display: inline-block;
            width: 80%;
            height: 25px;
        }
        
        .modal-body {
            height: 400px;
            overflow-y: scroll;
        }

        .dropdown-menu {
            width: 100%;
            overflow: scroll;
            height: 200px;
        }

        .inputs > div {
            padding-bottom: 10px;
        }

        span.input-group-addon {
            width: 25%;
        }
    `]
})
export class WellModalContent implements OnInit, AfterViewInit{ 
    @Input("well") well:Well;
    @Input("showBasicControllers") showBasicController;
    @Input("valueClicked") valueClicked = null;
    @Output("close") closeEvent = new EventEmitter();

    @Input() minvalue:number = 0;
    @Input() maxvalue:number = 100;

    constructor(private wellService:WellService, private changeRef:ChangeDetectorRef) {
        /*wellService.getVisualizationSettingsHandle().subscribe( (settings:any) => {
            this.minvalue = settings.minPropertyValue;
            this.maxvalue = settings.maxPropertyValue;
        });*/
    }

    close(e) {
        this.closeEvent.emit(true);
    }

    select_value_changed(i) {
        this.well.activeVisualizationIndex = i;
    }

    changeMinVal(e) {
        this.minvalue = e.target.value;
        this.changeRef.detectChanges();
    }

    changeMaxVal(e) {
        this.maxvalue = e.target.value;
        this.changeRef.detectChanges();
    }

    onClick(event) {
    }

    updateVisualizationSettings(e) {
        /*this.wellService.updateVisualizationSettings({
            activeVisualizationIndex: this.well.activeVisualizationIndex,
            minPropertyValue: this.minvalue,
            maxPropertyValue: this.maxvalue
        });*/

        this.well.minPropertyValue = this.minvalue;
        this.well.maxPropertyValue = this.maxvalue;
        this.wellService.changeSingleWell(this.well);
    }

    ngOnInit() {
        console.log("init");
        console.log(this.well);
        console.log(this.well.propertyNames[this.well.activeVisualizationIndex]);
    }

    ngAfterViewInit() {
        
        this.minvalue = this.well.minPropertyValue;
        this.maxvalue = this.well.maxPropertyValue;
        this.changeRef.detectChanges();

        var c:any=document.getElementById('canvas-legend');
        var ctx:any=c.getContext('2d');

        var grd=ctx.createLinearGradient(0,0,255,0);
        grd.addColorStop(0.0, '#7700ff');
        grd.addColorStop(0.25, '#0000ff');
        grd.addColorStop(0.5, '#00ff00');
        grd.addColorStop(0.75, '#ffff00');
        grd.addColorStop(1.0, '#FF0000');

        ctx.fillStyle=grd;
        ctx.fillRect(0,0,c.width,c.height);
    }
}