import { Component, ViewContainerRef, ViewChild, ReflectiveInjector, ComponentFactoryResolver, Input } from '@angular/core';
import { ModalComponent } from './modal.component';

@Component({
    selector: 'dynamic-component',
    entryComponents: [ModalComponent], // Reference to the components must be here in order to dynamically create them
    template: `
    <div #dynamicContainer></div>
  `,
})
export class DynamicComponent {
    currentComponent = null;
    @ViewChild('dynamicContainer', { read: ViewContainerRef }) dynamicComponentContainer: ViewContainerRef;

    constructor(private resolver: ComponentFactoryResolver) {
    }

    // component: Class for the component you want to create
    // inputs: An object with key/value pairs mapped to input name/input value
    @Input() set componentData(data: { component: any, inputs: any }) {
        console.log("Inside input setter");
        console.log(data);
        if (!data || !data.inputs) {
            if(this.currentComponent) this.currentComponent.destroy();
            return;
        }

        // Inputs need to be in the following format to be resolved properly
        let inputProviders = Object.keys(data.inputs).map(inputName => {
            return {
                provide: inputName,
                useValue: data.inputs[inputName]
            };
        });

        let resolvedInputs = ReflectiveInjector.resolve(inputProviders);

        // We create an injector out of the data we want to pass down and this components injector
        let injector = ReflectiveInjector.fromResolvedProviders(resolvedInputs, this.dynamicComponentContainer.parentInjector);

        // We create a factory out of the component we want to create
        let factory = this.resolver.resolveComponentFactory(data.component);

        // We create the component using the factory and the injector
        let component = factory.create(injector);

        console.log("Inserting component into host view");
        // We insert the component into the dom container
        this.dynamicComponentContainer.insert(component.hostView);

        // Destroy the previously created component
        if (this.currentComponent) {
            this.currentComponent.destroy();
        }

        this.currentComponent = component;
    }
}