import { Component, Input, ViewChild, AfterViewInit, Injector } from '@angular/core';
import { ModalService } from './modal.service';

@Component({
    selector: 'modal',
    template: `
        <div class='modal' tabindex="-1" role="dialog" aria-labelledby="modal-header">
            <div class="modal-dialog" role="document">
                
                <sliceModalContent *ngIf="showSlice" (close)="close($event)" [slice]="slice" [showBasicControllers]="showBasicControllers"></sliceModalContent>
                <wellModalContent *ngIf="showWell" (close)="close($event)" [well]="well" [valueClicked]="valueClicked" [showBasicControllers]="showBasicControllers"></wellModalContent>
                
                <div class="modal-content" *ngIf="showDefault">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-header">{{header}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" (click)="close($event)">
                            <span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{content}}
                    </div>
                    <div ng-if="showBasicControllers" class="modal-footer">
                        <button (click)="close($event)" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button (click)="save($event)"type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>

            <div class="overlay"></div>
        </div>
    `,
    styles: [`
        .modal-dialog {
            z-index: 101;
            display: block !important;
        }
        .modal {
            display: block !important;
        }

        .modal-body {
            height: 300px;
            overflow-y: scroll;
        }

        .overlay {
            position: fixed;
            background-color: rgba(0,0,0,0.40);
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            z-index: 100;
        }
    `]
})
export class ModalComponent implements AfterViewInit {

    public visible = false;
    private visibleAnimate = false;
    private showDefault = false;
    private showSlice = false;
    private showWell = false;
    header;
    content;
    showBasicControllers;
    slice;
    well;
    valueClicked;

    constructor(private injector: Injector, private modalService: ModalService) {
        console.log("modal constructor");
        this.header = this.injector.get("header");
        this.content = this.injector.get("content");
        this.showBasicControllers = this.injector.get("showBasicControllers");
        this.slice = this.injector.get("slice");
        this.well = this.injector.get("well");
        this.valueClicked = this.injector.get("valueClicked");

        // Decide what to show
        if(this.slice) {
            this.showSlice = true;
            this.showDefault = false;
            this.showWell = false;
        } else if(this.well) {
            this.showWell = true;
            this.showDefault = false;
            this.showSlice = false;
        }
    }

    ngAfterViewInit() {
        console.log(this.header);
        console.log(this.content);
        console.log(this.showBasicControllers);
    }

    close(e) {
        console.log("modal close called");
        this.modalService.hide();
    }

    save(e) {
        this.close(e);
    }
}