import { Component, Input,Output, EventEmitter} from '@angular/core';
import { ModalService } from './modal.service';

@Component({
    selector: 'sliceModalContent',
    template: `
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="modal-header">{{slice.name}}</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" (click)="close($event)">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="description">
                    <h5>Description</h5>
                    <p>{{slice.description}}</p>
                </div>
                <div class="related-articles" (click)="onClick($event)">
                    <h5 class="related-articles-header">Related Articles</h5>
                    <div class="related-articles-content">
                        <div class="list-group">
                            <a *ngFor="let item of slice.related_links" [href]="item" class="list-group-item list-group-item-action">{{item}}</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div ng-if="showBasicControllers" class="modal-footer">
                <button (click)="close($event)" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    `,
    styles: [`
        .related-articles {
            padding: 5px;
            height: 200px;
            overflow: scroll;
        }
        
        h5 {
            font-size: 18px;
            font-weight: 600;
        }
        .description {
            padding: 5px 5px 10px 5px;
            border-bottom: 3px solid #dddddd;
        }
    `]
})
export class SliceModalContent {
    @Input("showBasicControllers") showBasicController;
    @Input("slice") slice;
    @Output("close") closeEvent = new EventEmitter();

    constructor() {}

    close(e) {
        this.closeEvent.emit(true);
    }

    onClick(e) {
        console.log(this.slice);
    }
}