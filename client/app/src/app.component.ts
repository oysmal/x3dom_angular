import { Component, ViewContainerRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Overlay } from 'angular2-modal';
import { ModalService } from './modal/modal.service';
import { ModalComponent } from './modal/modal.component';


@Component({
    selector: 'my-app',
    template: `
        <router-outlet></router-outlet>
        <dynamic-component [componentData]="componentData"></dynamic-component>
    `
})
export class AppComponent {
    componentData;

    constructor(overlay: Overlay, vcRef: ViewContainerRef, private modalService:ModalService, private cdr:ChangeDetectorRef) {
        this.modalService.getUpdateHandle().subscribe(data => {
            this.componentData = {
                component: ModalComponent,
                inputs:data
            };
            this.cdr.detectChanges();
        });
    }
}