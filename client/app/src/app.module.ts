import { NgModule, Provider } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule }  from '@angular/forms';
import { RouterModule }  from '@angular/router';
import { HttpModule, JsonpModule }  from '@angular/http';


import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';


import { AppComponent } from './app.component';
import { MapViewComponent } from './mapview.component';
import { SidebarComponent} from './sidebar/sidebar.component';
import { BvhRefinerSettingsComponent } from './sidebar/bvhRefinerSettings/bvhRefinerSettings.component';
import { BvhRefinerSettingsService} from './x3d_components/bvhRefiner/bvhRefinerSettings.service';
import { TerrainComponent } from './x3d_components/terrain/terrain.component';
import { X3DOMComponent } from './x3d_components/x3d/x3d.component';
import { X3DOMUtilsService } from './x3d_components/x3d/x3dom_utils.service';
import { Slice2DComponent } from './x3d_components/slice2D/slice2d.component';
import { SliceGroupComponent } from './x3d_components/sliceGroup/sliceGroup.component';
import { BoxShapeComponent } from './x3d_components/boxShape/boxShape.component';
import { SubstituteGeometry } from './x3d_components/substituteGeometry/substituteGeometry.component';
import { FileReaderMultipleComponent } from './file/file_reader_multiple.component';

import { SliceLoaderService } from './x3d_components/sliceGroup/sliceLoader/sliceLoader.service';
import { SliceUploadService } from './upload_slice/slice_upload.service';
import { CoordinateConverterService } from './upload_slice/coordinate_converter/coordinate_converter.service';
import { AppSettings } from './settings/settings.service';
import { CSVUploaderComponent } from './csv_uploader/csv_uploader.component'; 
import { ModalService } from './modal/modal.service';
import { ModalComponent } from './modal/modal.component';
import { SliceModalContent } from './modal/slice_modal_content.component';
import { WellModalContent } from './modal/well_modal_content.component';
import { DynamicComponent } from './modal/dynamic.component';
import { Slice } from './slices/slice';
import { SliceService } from './slices/slice.service';
import { SliceControlComponent } from './slices/slice_control.component';
import { ViewpointComponent } from './x3d_components/viewpoints/viewpoint.component';
import { ViewpointService } from './x3d_components/viewpoints/viewpoint.service';

import { WellComponent } from './x3d_components/well/well.component';
import { WellService } from './x3d_components/well/well.service';
import { WellGroupComponent } from './x3d_components/well/wellgroup.component';
import { WellUploadComponent } from './x3d_components/well/wellUpload/well_upload.component';

import { CylinderShapeComponent } from './x3d_components/cylinderShape/cylinderShape.component';

import { SliceJSComponent } from './plain_javascript/slices.component';
import { WellJSComponent } from './plain_javascript/wells.component';

import {ModuleList} from './my_modules.component';
import {ApiConstants} from '../../.tmp/apiConstants';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    RouterModule.forRoot([
      {
        path: '',
        component: MapViewComponent
      },
      {
        path: 'csv_uploader',
        component: CSVUploaderComponent
      },
      {
        path: 'well_uploader',
        component: WellUploadComponent
      }
    ]),
    HttpModule,
    JsonpModule
  ],
  declarations: [
    AppComponent,
    MapViewComponent,
    SidebarComponent,
    BvhRefinerSettingsComponent,
    TerrainComponent,
    SliceJSComponent,
    WellJSComponent,
    X3DOMComponent,
    Slice2DComponent,
    BoxShapeComponent,
    SliceGroupComponent,
    SubstituteGeometry,
    FileReaderMultipleComponent,
    CSVUploaderComponent,
    ModuleList,
    ModalComponent,
    DynamicComponent,
    SliceControlComponent,
    ViewpointComponent,
    WellComponent,
    WellGroupComponent,
    CylinderShapeComponent,
    WellUploadComponent,
    SliceModalContent,
    WellModalContent
  ],
  providers: [
    BvhRefinerSettingsService,
    SliceLoaderService,
    SliceUploadService,
    CoordinateConverterService,
    AppSettings,
    ModalService,
    SliceService,
    ViewpointService,
    WellService,
    X3DOMUtilsService,
    {provide: ApiConstants, useValue: ApiConstants}
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }