import {Directive} from '@angular/core';


@Directive({
    selector: 'BVHRefiner'
})
class BVHRefiner {}

@Directive({
    selector: 'transform'
})
class Transform {}

@Directive({
    selector: 'x3d'
})
class X3d {}

@Directive({
    selector: 'appearance'
})
class Appearance {}

@Directive({
    selector: 'material'
})
class Material {}

@Directive({
    selector: 'shape'
})
class Shape {}

@Directive({
    selector: 'box'
})
class Box {}

@Directive({
    selector: 'cylinder'
})
class CylinderComponent {}

@Directive({
    selector: 'sphere'
})
class Sphere {}

@Directive({
    selector: 'scene'
})
class Scene {}

@Directive({
    selector: 'placeholder'
})
class Placeholder {}

@Directive({
    selector: 'anchor'
})
class Anchor {}

@Directive({
    selector: 'x3dshapenode'
})
class X3DShapeNode {}

@Directive({
    selector: 'ImageTexture'
})
class ImageTexture {}

@Directive({
    selector: 'Switch'
})
class SwitchNode {}

@Directive({
    selector: 'Viewpoint'
})
class ViewpointNode {}

@Directive({
    selector: 'orientation'
})
class OrientationNode {}

@Directive({
    selector: 'position'
})
class PositionNode {}

@Directive({
    selector: 'navigationInfo'
})
class NavigationInfo {}




export const ModuleList = [BVHRefiner, Transform, X3d, Appearance, Material, Shape, Box, CylinderComponent, Sphere, Scene, Placeholder, Anchor, X3DShapeNode, ImageTexture, SwitchNode, ViewpointNode, OrientationNode, PositionNode, NavigationInfo ];