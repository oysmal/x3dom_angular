import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Well } from './well';
import { ReplaySubject } from 'rxjs';
import { ApiConstants } from '../../../../.tmp/apiConstants';

@Injectable()
export class WellService {
    private wells;
    private subject = new ReplaySubject<Array<Well>>(1);
    private visualization_settings_subject = new ReplaySubject<Object>(1);
    private wellUrl = ApiConstants.api + '/well';
    private single_well_change_subject = new ReplaySubject<Well>(1);

    getVisualizationSettingsHandle() {
        return this.visualization_settings_subject;
    }
    
    updateVisualizationSettings(obj) {
        this.visualization_settings_subject.next({
            minPropertyValue: obj.minPropertyValue,
            maxPropertyValue: obj.maxPropertyValue,
            activeVisualizationIndex: obj.activeVisualizationIndex
        });
    }

    changeSingleWell(well:Well) {
        this.wells[well.index.toFixed()] = well;
        this.single_well_change_subject.next(well);
        this.subject.next(this.wells);
    }

    getChangeSingleWellHandle() {
        return this.single_well_change_subject;
    }

    constructor(private http:Http) {

        this.getWells().subscribe( (wells:any) => {
            this.wells = wells.map( (well, i) => {
                well.id = well._id;
                well.activeVisualizationIndex = 2;
                well.minPropertyValue = 0;
                well.maxPropertyValue = 500;
                well.visible = true;
                well.index = i;
                return new Well(well);
            });
            this.subject.next(this.wells);
        });

        this.visualization_settings_subject.next({
            minPropertyValue: 0,
            maxPropertyValue: 500,
            activeVisualizationIndex: 2
        });
    }

    getSubscriptionHandle() {
        return this.subject;
    }

    getWells() {
        return this.http.get(this.wellUrl)
            .map((res: Response) => {
                let body = res.json();
                return body || {};
            }).catch((err: any) => {
                console.log(err);
                return err;
            });
    }

    uploadWell(well:Well) {
        return this.http.post(this.wellUrl, well).map((res: Response) => {
                console.log(res);
                let b = res.json();
                return b || {};
            }).catch((err: Response | any) => {
                return err.json();
            });
    }
}