export class Well {
    public id: string;
    public name: string;
    public description: string;
    public positions: Array<number>;
    public positions_row_length: number;
    public propertyNames: Array<string>;
    public propertyValues: Array<number>;
    public property_values_row_length: number;
    public activeVisualizationIndex: number;
    public minPropertyValue: number;
    public maxPropertyValue: number;
    public visible: boolean;
    public index: number;
    public related_links: Array<string>;

    constructor(obj?:any) {
        this.id = obj.id;
        this.name = obj.name;
        this.description = obj.description;
        this.positions = obj.positions;
        this.positions_row_length = obj.positions_row_length;
        this.propertyNames = obj.propertyNames;
        this.propertyValues = obj.propertyValues;
        this.property_values_row_length = obj.property_values_row_length;
        this.activeVisualizationIndex = obj.activeVisualizationIndex;
        this.minPropertyValue = obj.minPropertyValue;
        this.maxPropertyValue = obj.maxPropertyValue;
        this.visible = obj.visible;
        this.index = obj.index;
        this.related_links = obj.related_links;
    }
}