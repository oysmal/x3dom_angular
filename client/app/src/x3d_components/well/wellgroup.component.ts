import { Component, ChangeDetectorRef, AfterContentInit, OnDestroy, OnChanges, ViewChild, ElementRef } from '@angular/core';
import { WellService } from './well.service';
import { Well } from './well';

@Component({
    selector: '[wellgroup]',
    template: `
        <transform *ngFor="let wellInfo of wells">
            <transform well3D [well]="wellInfo"></transform>
        </transform>
    `
})
export class WellGroupComponent implements AfterContentInit, OnDestroy, OnChanges{
    private wells;

    constructor(private el:ElementRef, private wellService:WellService, private cdr:ChangeDetectorRef) {
        console.log("WellGroupComponent");
        this.wellService.getSubscriptionHandle().subscribe(x => {
            console.log("wellgroup: ");
            this.wells = x;
            console.log(this.wells);
            
            console.log("Updated");
            this.cdr.markForCheck();
            /*this.cdr.detectChanges();
            console.log("detected changes");*/
        });

        /*this.wellService.getVisualizationSettingsHandle().subscribe(obj => {
            console.log("update wellgroup wellVisualizationSettings");
            this.wellVisualizationSettings = obj;
        });*/
    }
    
    ngAfterContentInit() {
        console.log("WG AfterContentInit");
        console.log(this.wells);
        if(!this.el.nativeElement._listeners)
            this.el.nativeElement._listeners = [];
    }

    ngOnDestroy() {
        console.log("on destroy init");
        if(!this.el.nativeElement._listeners)
            this.el.nativeElement._listeners = [];
    }

    ngOnChanges(changes:any) {
        console.log(changes);
    }
}