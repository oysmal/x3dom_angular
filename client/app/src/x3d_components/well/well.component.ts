import {Component, Input, AfterContentInit, ChangeDetectorRef, OnChanges, OnDestroy, ElementRef} from '@angular/core';
import {Well} from './well';
import {WellService} from './well.service';
import { ModalService } from '../../modal/modal.service';

@Component({
    selector: '[well3D]',
    template: `
        <transform *ngFor="let pos of positions; let i = index">
            <transform [attr.translation]="pos" [attr.rotation]="rotation" cylinderShape (clicked)="onClick($event)" [diffuseColor]="colors[i]" [depth]="depths[i]"><transform>
        </transform>
    `
})
export class WellComponent implements AfterContentInit {
    @Input('well') well:Well;
    //@Input('minPropertyValue') minPropertyValue = 0;
    //@Input('maxPropertyValue') maxPropertyValue = 500;
    //@Input('activePropertyIndex') activePropertyIndex = 2;
    private positions:Array<string> = [];
    private depths:Array<string> = [];
    private color = "1,0,0";
    private colors = [];
    private rotation = [1, 0, 0, Math.PI/2]; // 90 degree rotation around x-axis to get vertical position
    private canvas:any;
    private ctx:any;
    private minPropertyValue;
    private maxPropertyValue;
    private activeVisualizationIndex;
    private hasLoaded = false;
    private visible;

    constructor(private el:ElementRef, private ref:ChangeDetectorRef, private modalService:ModalService, private wellService:WellService) {
        console.log("Well.component");

        this.wellService.getVisualizationSettingsHandle().subscribe((visSettings:any) => {
            this.minPropertyValue = visSettings.minPropertyValue;
            this.maxPropertyValue = visSettings.maxPropertyValue;
            this.activeVisualizationIndex = visSettings.activeVisualizationIndex;
            
            if (this.hasLoaded)
                this.setupVisualization();
        })
        

		// canvas setup
        this.canvas = document.createElement('canvas');     // create canvas element
        this.ctx = this.canvas.getContext('2d');                 // get context
        let gr = this.ctx.createLinearGradient(0, 0, 101, 0);   // create a gradient
        let i = 0, cs;

        this.canvas.width = 101;                                // 101 pixels incl.
        this.canvas.height = 1;                                 // as the gradient
        gr.addColorStop(0.0, '#000000');
        gr.addColorStop(1.0, '#FF0000');

        this.ctx.fillStyle = gr;                                // set as fill style
        this.ctx.fillRect(0, 0, 101, 1);
    }

    onClick(event) {
        console.log(event);
        let clickedValue = Number(event.color[0]*(this.maxPropertyValue-this.minPropertyValue)).toFixed(2);

        if(event.button == 1) {
            this.visible = !this.well.visible;
            this.ref.detectChanges();
        } else {
            this.modalService
                .reset()
                .setShowBasicControllers(true)
                .setWell(this.well)
                .setValueClicked(clickedValue)
                .show();
        }
    }

    ngAfterContentInit() {
        console.log("ngAfterContentInit");
        this.hasLoaded = true;
        this.setupVisualization();
    }


    setupVisualization() {
        let i_chronologically = 0;
        for(let i = 0; i < this.well.positions.length-3; i+=this.well.positions_row_length) {
            let pos = [this.well.positions[i], this.well.positions[i+1], this.well.positions[i+2]];

            let next_pos = [this.well.positions[i+this.well.positions_row_length], 
                            this.well.positions[i+this.well.positions_row_length+1], 
                            this.well.positions[i+this.well.positions_row_length+2]];

            let newpos = [pos[0]+(next_pos[0]-pos[0])/2, pos[1]+(next_pos[1]-pos[1])/2, -(next_pos[2]+pos[2])/2];
            this.positions.push(newpos.join(','));
            this.depths.push((next_pos[2]-pos[2]).toString());

            let propValue = this.well.propertyValues[this.activeVisualizationIndex + i_chronologically*this.well.property_values_row_length];
            console.log("Prop: " + this.well.propertyNames[this.activeVisualizationIndex] + ": " + propValue);
            let color = "0,0,0";
            if(propValue && propValue > 0) {
                let propertyValueRange = this.maxPropertyValue-this.minPropertyValue;
                let gradientPos = propValue/propertyValueRange*100;
                console.log(gradientPos);
                let tmp = this.ctx.getImageData(gradientPos, 0, 1, 1).data;
                console.log(tmp);
                color = [parseFloat(tmp[0])/255.0, parseFloat(tmp[1])/255.0, parseFloat(tmp[2])/255.0].join(',');
            }
            console.log(color);
            this.colors.push(color);
            i_chronologically++;
        }
    }
}