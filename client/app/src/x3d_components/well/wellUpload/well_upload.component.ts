import { Component } from '@angular/core';
import { WellService } from '../well.service';
import { Well } from '../well';
import { CoordinateConverterService } from '../../../upload_slice/coordinate_converter/coordinate_converter.service';
import * as proj4 from 'proj4';

import * as Papa from 'papaparse';

@Component({
    selector: 'well-uploader',
    templateUrl: './well_upload.template.html',
    styles: [`
        body {
            background-color: white;
        }
    `]
})
export class WellUploadComponent {
    csvdata;
    fileTarget;
    disabled = true;
    rms_disabled = true;
    files;
    wsg84proj = 'EPSG:4326';
    utmProj = '+proj=utm +zone=33X +ellps=GRS80 +units=m +no_defs';
    csv;

    constructor(private coordinateConverterService: CoordinateConverterService, private wellService:WellService) { }

    complete(files) {
        this.createProperWellDataObjectsFromResult(this.csv, files);
    }

    error(event) {
        console.log(event);
    }

    upload() {
        this.readFile(this.fileTarget);
    }

    fileChange($event) {
        this.fileTarget = $event.target;
        this.readFile(this.fileTarget);
        this.disabled = false;
    }

    wellLogChange($event) {
        this.files = $event.target.files;
    }

    readFile(inputValue: any): void {
        let file: File = inputValue.files[0];

        let config = {
            delimiter: "",	// auto-detect
            newline: "",	// auto-detect
            header: true,
            dynamicTyping: false,
            preview: 0,
            encoding: "",
            worker: false,
            comments: false,
            step: undefined,
            complete: (result, file) => {
                this.csv = result.data;
                this.rms_disabled = false;
                //this.createProperWellDataObjectsFromResult(result.data);
            },
            error: (err, file) => {
                console.log(err);
            },
            download: false,
            skipEmptyLines: false,
            chunk: undefined,
            fastMode: undefined,
            beforeFirstChunk: undefined,
            withCredentials: undefined
        }

        Papa.parse(file, config);
    }

    createProperWellDataObjectsFromResult(data:any, files) {

        // loop through each element of "data"
        data.map((x, i) => {
            if(x.data_type.indexOf("Well")) return;
            if(!x.additional_files) return;

            // Create array containing names of the rms files related to the current element of data
            let relevant_rms_filename = x.additional_files.split(',').filter( a => {
                return (/^.+.rms$/).exec(a);
            })[0];

            let rms_file = null;

            // loop through all files that were selected for potential upload
            for(let i = 0; i < files.length; i++) {
                // if the current file's name == relevant_rms_filename,
                // set the file to upload
                if(files[i].name == relevant_rms_filename) {
                    rms_file = files[i].contents;
                }
            }
            if(!rms_file) return;

            let rms_data = this.readRMSFile(rms_file);

            let well = new Well({
                name: x.dataset_name,
                description: x.info,
                positions: rms_data.positions,
                positions_row_length: rms_data.positions_row_length,
                propertyNames: rms_data.propertyNames,
                propertyValues: rms_data.propertyValues,
                property_values_row_length: rms_data.property_values_row_length,
                related_links: x.related_links.split(','),
                visible: true
            });

            
            this.wellService.uploadWell(well).subscribe(x => {
                console.log(x);
            },
            errors => {
                console.log(errors);
            });
        });
    }

    /**
     * Reads a rms file and returns 
     * @param rmsFile the text content of an rms file
     */
    readRMSFile(rmsFile) {
        let lines = rmsFile.split('\n');    // all lines in the file
        let numProperties = parseInt(lines[3]); // number of properties
        let propertyNames:Array<string> = []; // list of the property names

        // Read property names
        for(let i = 4; i<numProperties+4-1; i++) {
            propertyNames.push(lines[i]);
        }

        // create increment value so maximum samples is 100
        let increment = Math.floor((lines.length-numProperties-4)/100);  // make sure to take 100 samples.
        if(increment < 1) 
            increment = 1;

        let propertyValues:Array<number> = new Array<number>(numProperties); // 1d array of all the properties
        let positions:Array<number> = new Array<number>(); // the 1d array of positions (first 3 values in each line)

        // Read up to 100 of the lines of properties
        for(let i = 4+numProperties; i<lines.length; i+=increment) {
            let propValues = lines[i].split(' '); // line split by space to get values

            // push positions (x,y,z)
            if(propValues[0] && propValues[1] && propValues[2]) {
                let pos = this.coordinateConverterService.getPositionInLocalCoordinateSystem(parseFloat(propValues[0]), parseFloat(propValues[1]));
                let depth = this.coordinateConverterService.getDepthInLocalCoordinateSystem(parseFloat(propValues[2]));
                positions.push(pos.x);
                positions.push(pos.y);
                positions.push(depth);
            }
            
            if(propValues.length != (numProperties+3)) break; // a line of values must be as long as the numProperties +3 (because 3 first values is position)

            // for each value of this line
            for(let j = 3; j < propValues.length; j++) {
                propertyValues.push(parseFloat(propValues[j]));  // push each propvalue of this line to propertyValues
            }
        }

        // Return the data read from the RMS data
        return {
            positions: positions,
            positions_row_length: 3,
            propertyNames: propertyNames,
            propertyValues: propertyValues,
            property_values_row_length: numProperties
        }
        
    }
}