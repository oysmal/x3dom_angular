import { Component, ViewChild, Input, OnInit, AfterViewInit, AfterContentInit, OnDestroy, ElementRef, Output, EventEmitter } from '@angular/core';
import { X3DOMUtilsService } from '../x3d/x3dom_utils.service';

declare var x3dom: any;

@Component({
    selector: '[substitute]',
    template: `
        <transform>
            <shape id="shape" #shape >
                <appearance>
                    <ImageTexture [attr.url]="bg_image_url"></ImageTexture>
                
                </appearance>
                <sphere id="tester" [attr.radius]="radius"></sphere>
            </shape>
        </transform>
    `
})
export class SubstituteGeometry implements AfterViewInit, OnDestroy {
    bg_image_url = "/resources/red_opaque_500x500.png";
    radius = "1";
    @ViewChild("shape") shape;
    @Output("clicked") clicked = new EventEmitter();
    
    constructor(private elementRef: ElementRef, private x3domUtilsService:X3DOMUtilsService) {
        console.log("substitute");
    }

    ngAfterViewInit() {
        this.x3domUtilsService.load.subscribe( e => {
            this.shape.nativeElement.addEventListener('click', this.onClick.bind(this));
        });
    }
    ngOnDestroy() {
        console.log("destroy substitute");
    }

    onClick(event) {
        this.clicked.emit(event);
    }
}