System.register(['angular2/core', 'rxjs/Rx'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, Rx_1;
    var BvhRefinerSettingsService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (Rx_1_1) {
                Rx_1 = Rx_1_1;
            }],
        execute: function() {
            BvhRefinerSettingsService = (function () {
                function BvhRefinerSettingsService() {
                    this.settings = {
                        maxDepth: 0,
                        minDepth: 0,
                        interactionDepth: 0,
                        smoothLoading: 0,
                        subdivision: "256 256",
                        size: "1024 1024",
                        factor: 10,
                        maxElevation: 5,
                        elevationUrl: "app/resources/elevation",
                        textureUrl: "",
                        normalUrl: "",
                        elevationFormat: "png",
                        textureFormat: "jpg",
                        normalFormat: "jpg",
                        mode: "3d"
                    };
                    this.updates = new Rx_1.default.ReplaySubject(1);
                    console.log("Service created");
                }
                BvhRefinerSettingsService.prototype.updateSettings = function (settings) {
                    this.settings = settings;
                    this.updates.next(this.settings);
                };
                BvhRefinerSettingsService.prototype.getSettings = function () {
                    return this.settings;
                };
                BvhRefinerSettingsService.prototype.getUpdateHandle = function () {
                    return this.updates;
                };
                BvhRefinerSettingsService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [])
                ], BvhRefinerSettingsService);
                return BvhRefinerSettingsService;
            }());
            exports_1("BvhRefinerSettingsService", BvhRefinerSettingsService);
        }
    }
});
//# sourceMappingURL=bvhRefinerSettings.service.js.map