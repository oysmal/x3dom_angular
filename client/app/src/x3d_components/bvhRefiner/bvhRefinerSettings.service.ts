import {Injectable} from '@angular/core';
import {ReplaySubject} from 'rxjs/Rx';
import {AppSettings} from '../../settings/settings.service';

@Injectable()
export class BvhRefinerSettingsService {
    settings = {
        maxDepth : 0,
        minDepth : 0,
        interactionDepth : 0,
        smoothLoading : 0,
        subdivision : "256 256",
        size : "1024 1024",
        factor : 10, 
        maxElevation : 5,
        elevationUrl : "",
        textureUrl : "",
        normalUrl : "",
        elevationFormat : "png",
        textureFormat : "png",
        normalFormat : "png",
        mode : "3d",
        latLongTopLeft: "",
        latLongTopRight: "",
    };

    updates = new ReplaySubject(1);

    constructor() {
        console.log("Service created");
        this.settings.size = AppSettings.width + ',' + AppSettings.height;
    }
    

    updateSettings(settings) {
        this.settings = settings;
        this.updates.next(this.settings);
    }

    getSettings() {
        return this.settings;
    }

    getUpdateHandle() {
        return this.updates;
    }
}