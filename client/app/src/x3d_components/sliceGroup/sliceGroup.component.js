System.register(['angular2/core', '../slice2D/slice2D.component', './SliceLoader/sliceLoader.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, slice2D_component_1, sliceLoader_service_1;
    var SliceGroupComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (slice2D_component_1_1) {
                slice2D_component_1 = slice2D_component_1_1;
            },
            function (sliceLoader_service_1_1) {
                sliceLoader_service_1 = sliceLoader_service_1_1;
            }],
        execute: function() {
            SliceGroupComponent = (function () {
                function SliceGroupComponent(sliceLoaderService) {
                    this.sliceLoaderService = sliceLoaderService;
                    this.settings = {
                        position: "0 0 0",
                        rotation: "0 0 0 0",
                        textureUrl: null
                    };
                }
                SliceGroupComponent.prototype.ngAfterViewInit = function () {
                    x3dom.reload();
                    this.sliceLoaderService.loadSlices();
                };
                SliceGroupComponent = __decorate([
                    core_1.Component({
                        selector: '[sliceGroup]',
                        template: "\n        <slice2D [attr.position]=\"settings.position\" [attr.rotation]=\"settings.rotation\" slice2D [attr.textureUrl]=\"settings.textureUrl\"></slice2D>\n    ",
                        providers: [sliceLoader_service_1.SliceLoaderService],
                        directives: [slice2D_component_1.Slice2DComponent]
                    }), 
                    __metadata('design:paramtypes', [sliceLoader_service_1.SliceLoaderService])
                ], SliceGroupComponent);
                return SliceGroupComponent;
            }());
            exports_1("SliceGroupComponent", SliceGroupComponent);
        }
    }
});
//# sourceMappingURL=sliceGroup.component.js.map