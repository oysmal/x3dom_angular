import { Component, Input, ChangeDetectorRef, DoCheck, OnDestroy, OnChanges, SimpleChanges, ElementRef, ViewChildren, QueryList } from '@angular/core';
import { Slice2DComponent } from '../slice2D/slice2d.component';
import { SliceService } from '../../slices/slice.service';
import { Slice } from '../../slices/slice';
import { ApiConstants } from '../../../../.tmp/apiConstants';
import { ViewpointService } from '../viewpoints/viewpoint.service';

declare var x3dom: any;

@Component({
    selector: '[sliceGroup]',
    template: `
    <transform *ngIf="slices && slices.length > 0">
        <transform *ngFor="let s of slices; let i = index;">
            <transform slice2D [slice]="s"></transform>
            <transform viewpoint [slice]="s"></transform>
        </transform>
    </transform>
    `
})
export class SliceGroupComponent implements OnDestroy, OnChanges {
    slices;

    constructor(private el:ElementRef,
                private sliceService: SliceService, 
                private viewpointService:ViewpointService, 
                private cdr:ChangeDetectorRef) {
        this.loadData();
    }

    ngOnDestroy() {
        console.log("destroy slicegroup");
    }

    ngOnChanges(changes:SimpleChanges) {
        console.log("changing the slicegroup");
    }

    loadData() {
        let subscription = this.sliceService.slices.subscribe(slices => {
            console.log("once");
            this.slices = slices;
            this.cdr.markForCheck();
            subscription.unsubscribe();
        });
    }
}