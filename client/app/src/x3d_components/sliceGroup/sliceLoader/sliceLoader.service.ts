import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import { ApiConstants } from '../../../../../.tmp/apiConstants';
import { Observable }     from 'rxjs/Observable';

@Injectable()
export class SliceLoaderService {

    private sliceResourceUrl = ApiConstants.api + "/slice_model";

    constructor(private http: Http) {
        //console.log(this.sliceResourceUrl);
    }

    loadSlices() {
        //console.log("loadslices");
        return this.http.get(this.sliceResourceUrl)
            .map((res: Response) => {
                let body = res.json();
                body.map( (x, i) => {
                    x.index = i;
                    return x;
                });
                console.log(body);
                //console.log("sliceloader res");
                //console.log(res);
                return body || {};
            }).catch((err: any) => {
                //console.log("ERRRR¤¤¤¤¤¤¤");
                //console.log(err);
                return err;
            });
    }
}