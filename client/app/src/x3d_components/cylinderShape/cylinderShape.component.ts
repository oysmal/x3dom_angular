import { Component, Input, AfterContentInit, ElementRef, ChangeDetectorRef, Output, EventEmitter, ViewChild, OnDestroy } from '@angular/core';
import { X3DOMUtilsService } from '../x3d/x3dom_utils.service';

declare var x3dom: any;
declare var document: any;

@Component({
    selector: '[cylinderShape]',
    template: `
        <transform>
            <shape #shape>
                <appearance>
                    <material [attr.diffuseColor]="diffuseColor"></material>
                </appearance>
                <cylinder [attr.radius]="radius" [attr.height]="depth"></cylinder>
            </shape>
        </transform>
    `
})
export class CylinderShapeComponent implements AfterContentInit, OnDestroy {

    @Input("diffuseColor") diffuseColor = '1,0,0';
    @Input("depth") depth = "2";
    @Input("rotation") rotation = "0,0,0,0";
    @Output("clicked") clicked = new EventEmitter();
    @ViewChild("shape") shape;

    radius:string = "1";


    constructor(private elementRef: ElementRef, private x3domUtilsService:X3DOMUtilsService) {
    }

    ngAfterContentInit() {
        console.log(this.depth);
        
        this.x3domUtilsService.load.subscribe( e => {
            console.log("Cylindershape ready");
            this.shape.nativeElement.addEventListener('click', this.onClick.bind(this));
        });
    }

    onClick(event) {
        console.log("click cylinder");
        this.clicked.emit({
            color: this.diffuseColor.split(',').map(x => parseFloat(x)),
            button: event.button
        });
    }

    ngOnDestroy() {
        console.log("cylinder destroy");
        console.log(this.elementRef);
    }
}