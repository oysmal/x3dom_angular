import { Component, AfterViewInit, ElementRef } from '@angular/core';
import { BoxShapeComponent } from '../boxShape/boxShape.component';
import { TerrainComponent } from '../terrain/terrain.component';
import { BvhRefinerSettingsService } from '../bvhRefiner/bvhRefinerSettings.service';
import { SliceGroupComponent } from '../sliceGroup/sliceGroup.component';
import { Slice2DComponent } from '../slice2D/slice2d.component';
import { SidebarComponent } from '../../sidebar/sidebar.component';
import { HostListener } from '@angular/core';
import { AppSettings } from '../../settings/settings.service';
import { X3DOMUtilsService } from './x3dom_utils.service';

declare var x3dom: any;

@Component({
	selector: 'x3dom',
	template: `
	<x3d id="x3d" class="x3d" width="100%" height="100%">
        <scene>

			<transform slicejs></transform>
			<transform welljs></transform>
            <transform terrain></transform>

			<!--<transform sliceGroup></transform>-->
			<!--<transform wellgroup></transform>-->
			<!--<transform subterrain></transform>-->
			
        </scene>
    </x3d>
	
	<sidebar></sidebar>
	`
})
export class X3DOMComponent implements AfterViewInit {

	elem;
	showSidebar = false;
	bvhRefinerSettings;

	constructor(elem: ElementRef, bvhRefinerSettings: BvhRefinerSettingsService, private x3domUtilsService:X3DOMUtilsService) {
		this.elem = elem;
		this.bvhRefinerSettings = bvhRefinerSettings;
		this.bvhRefinerSettings.updateSettings({
			maxDepth: 2,
			minDepth: 0,
			interactionDepth: 2,
			smoothLoading: 5,
			subdivision: "128 128",
			size: AppSettings.width + ',' + AppSettings.height,
			factor: 10,
			maxElevation: 5,
			elevationUrl: "resources/elevation",
			textureUrl: "resources/textures",
			normalUrl: "resources/textures",
			elevationFormat: "png",
			textureFormat: "png",
			normalFormat: "png",
			mode: "3d"
		});
	}

	ngAfterViewInit() {

		// dispatch the ready event from x3dom to all listeners
		x3dom.runtime.ready = () => {
			//this.x3domUtilsService.triggerLoadEvent();
			let elem = document.querySelector('.x3d') as any;
			//elem.runtime.debug (true);
			elem.runtime.speed(15);
		};
	}

	/*@HostListener("window:keydown", ['$event'])
	onKeyPressed(event: any) {
		if (event.keyCode == 80) {
			this.showSidebar = !this.showSidebar;
		}
	}*/

}