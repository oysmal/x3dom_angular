System.register(['angular2/core', '../terrain/terrain.component', '../skybox/skybox.component', '../bvhRefiner/bvhRefinerSettings.service', '../sliceGroup/sliceGroup.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, terrain_component_1, skybox_component_1, bvhRefinerSettings_service_1, sliceGroup_component_1;
    var X3DOMComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (terrain_component_1_1) {
                terrain_component_1 = terrain_component_1_1;
            },
            function (skybox_component_1_1) {
                skybox_component_1 = skybox_component_1_1;
            },
            function (bvhRefinerSettings_service_1_1) {
                bvhRefinerSettings_service_1 = bvhRefinerSettings_service_1_1;
            },
            function (sliceGroup_component_1_1) {
                sliceGroup_component_1 = sliceGroup_component_1_1;
            }],
        execute: function() {
            X3DOMComponent = (function () {
                function X3DOMComponent(elem, bvhRefinerSettings) {
                    this.skyboxSettings = {
                        topUrl: 'app/resources/skybox_sunny/sunny_up.png',
                        bottomUrl: 'app/resources/skybox_sunny/sunny_down.png',
                        leftUrl: 'app/resources/skybox_sunny/sunny_left.png',
                        rightUrl: 'app/resources/skybox_sunny/sunny_right.png',
                        backUrl: 'app/resources/skybox_sunny/sunny_back.png',
                        frontUrl: 'app/resources/skybox_sunny/sunny_front.png'
                    };
                    this.elem = elem;
                    this.bvhRefinerSettings = bvhRefinerSettings;
                    this.bvhRefinerSettings.updateSettings({
                        maxDepth: 4,
                        minDepth: 0,
                        interactionDepth: 4,
                        smoothLoading: 5,
                        subdivision: "256 256",
                        size: "1024 1024",
                        factor: 10,
                        maxElevation: 5,
                        elevationUrl: "app/resources/elevation",
                        textureUrl: "app/resources/textures",
                        normalUrl: "app/resources/textures",
                        elevationFormat: "png",
                        textureFormat: "jpg",
                        normalFormat: "jpg",
                        mode: "3d"
                    });
                }
                X3DOMComponent.prototype.onClick = function ($event) {
                };
                X3DOMComponent.prototype.ngAfterViewInit = function () {
                    setTimeout(function () {
                        setTimeout(function () {
                            var elem = document.querySelector('.x3d');
                            elem.runtime.speed(10);
                        }, 5000);
                        x3dom.reload();
                    }, 5000);
                };
                X3DOMComponent = __decorate([
                    core_1.Component({
                        selector: 'x3dom',
                        template: "\n\t<x3d class=\"x3d\" width=\"100%\" height=\"100%\">\n        <scene>\n\n\t\t\t<transform sliceGroup></transform>\n            <transform terrain></transform>\n\n\t\t\t<!--<transform skybox [backUrl]=\"skyboxSettings.backUrl\" [frontUrl]=\"skyboxSettings.frontUrl\" [topUrl]=\"skyboxSettings.topUrl\" \n\t\t\t\t[bottomUrl]=\"skyboxSettings.bottomUrl\" [leftUrl]=\"skyboxSettings.leftUrl\" [rightUrl]=\"skyboxSettings.rightUrl\">\n\t\t\t</transform>-->\n\n        </scene>\n    </x3d>\n\t",
                        styles: ["\n\t\t.x3d {\n\t\t\twidth: 100%;\n\t\t\theight: 100%;\n\t\t\tborder: 1px solid black;\n\t\t}\n\t"],
                        directives: [terrain_component_1.TerrainComponent, skybox_component_1.SkyboxComponent, sliceGroup_component_1.SliceGroupComponent]
                    }), 
                    __metadata('design:paramtypes', [core_1.ElementRef, bvhRefinerSettings_service_1.BvhRefinerSettingsService])
                ], X3DOMComponent);
                return X3DOMComponent;
            }());
            exports_1("X3DOMComponent", X3DOMComponent);
        }
    }
});
//# sourceMappingURL=x3d.component.js.map