import {Injectable} from '@angular/core';
import {ReplaySubject} from 'rxjs';

declare var $:any;

@Injectable()
export class X3DOMUtilsService {
    public load:ReplaySubject<boolean> = new ReplaySubject<boolean>(1);

    public triggerLoadEvent() {
        this.load.next(true);
        console.log("x3dom_loaded");
        $(document).trigger('x3dom_loaded');
    }
}