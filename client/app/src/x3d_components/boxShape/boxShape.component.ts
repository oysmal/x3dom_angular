import { Component, Input, AfterContentInit, ElementRef, ChangeDetectorRef, Output, EventEmitter, ViewChild, OnDestroy } from '@angular/core';
import { X3DOMUtilsService } from '../x3d/x3dom_utils.service';

declare var x3dom: any;
declare var document: any;

@Component({
    selector: '[boxShape]',
    template: `
        <transform>
            <shape id="shape" #shape>
                <appearance>
                    <ImageTexture [attr.url]="textureUrl"></ImageTexture>
                
                </appearance>
                <box id="tester" [attr.size]="size"></box>
            </shape>
        </transform>
    `
})
export class BoxShapeComponent implements AfterContentInit, OnDestroy {

    @Input("diffuseColor") diffuseColor = '1,0,0';
    @Input("box_textureUrl") textureUrl = null;
    @Input("box_size") size = "2,0.1,2";
    @Output("clicked") clicked = new EventEmitter();
    @ViewChild("shape") shape;
    substitute = "/resources/substitute.png";
    @Input("show") show;


    constructor(private elementRef: ElementRef, private x3domUtilsService:X3DOMUtilsService) {}

    ngAfterContentInit() {
        this.x3domUtilsService.load.subscribe( e => {
            console.log("Boxshape ready");
            this.shape.nativeElement.addEventListener('click', this.onClick.bind(this));
            console.log(this.shape);
        });
    }

    ngOnDestroy() {
        console.log("destroy boxshape");
    }

    onClick(event) {
        this.clicked.emit(event);
    }
}