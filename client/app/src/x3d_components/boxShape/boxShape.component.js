System.register(['angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var BoxShapeComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            BoxShapeComponent = (function () {
                function BoxShapeComponent(elem) {
                    this.diffuseColor = '1 0 0';
                    this.url = null;
                    this.size = "2 2 10";
                    this.elem = elem;
                    console.log(this.diffuseColor);
                }
                BoxShapeComponent.prototype.onClick = function () {
                };
                BoxShapeComponent.prototype.ngAfterViewInit = function () {
                    this.elem.nativeElement.onclick = this.onClick; // bind x3dom onclick to this class's onClick method
                };
                __decorate([
                    core_1.Input("diffuseColor"), 
                    __metadata('design:type', Object)
                ], BoxShapeComponent.prototype, "diffuseColor", void 0);
                __decorate([
                    core_1.Input("textureUrl"), 
                    __metadata('design:type', Object)
                ], BoxShapeComponent.prototype, "url", void 0);
                __decorate([
                    core_1.Input("size"), 
                    __metadata('design:type', Object)
                ], BoxShapeComponent.prototype, "size", void 0);
                BoxShapeComponent = __decorate([
                    core_1.Component({
                        selector: '[boxShape]',
                        template: "\n        <shape>\n            <appearance>\n                <material *ngIf=\"!url\" [attr.diffuseColor]=\"diffuseColor\"></material>\n               \n            </appearance>\n            <box [attr.size]=\"size\"></box>\n        </shape>\n    "
                    }), 
                    __metadata('design:paramtypes', [core_1.ElementRef])
                ], BoxShapeComponent);
                return BoxShapeComponent;
            }());
            exports_1("BoxShapeComponent", BoxShapeComponent);
        }
    }
});
//# sourceMappingURL=boxShape.component.js.map