System.register(['angular2/core', '../boxShape/boxShape.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, boxShape_component_1;
    var Slice2DComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (boxShape_component_1_1) {
                boxShape_component_1 = boxShape_component_1_1;
            }],
        execute: function() {
            Slice2DComponent = (function () {
                function Slice2DComponent() {
                    this.textureUrl = null;
                    this.translation = "0 0 0";
                    this.rotation = "0 0 0";
                    this.size = "2 2 10";
                    console.log("Slice2DComponent created");
                }
                __decorate([
                    core_1.Input("textureUrl"), 
                    __metadata('design:type', Object)
                ], Slice2DComponent.prototype, "textureUrl", void 0);
                __decorate([
                    core_1.Input("translation"), 
                    __metadata('design:type', Object)
                ], Slice2DComponent.prototype, "translation", void 0);
                __decorate([
                    core_1.Input("rotation"), 
                    __metadata('design:type', Object)
                ], Slice2DComponent.prototype, "rotation", void 0);
                __decorate([
                    core_1.Input("size"), 
                    __metadata('design:type', Object)
                ], Slice2DComponent.prototype, "size", void 0);
                Slice2DComponent = __decorate([
                    core_1.Component({
                        selector: '[slice2D]',
                        template: "\n        <transform [attr.translation]=\"translation\" [attr.rotation]=\"rotation\" boxShape [textureUrl]=\"textureUrl\" [size]=\"size\"></transform>\n    ",
                        directives: [boxShape_component_1.BoxShapeComponent]
                    }), 
                    __metadata('design:paramtypes', [])
                ], Slice2DComponent);
                return Slice2DComponent;
            }());
            exports_1("Slice2DComponent", Slice2DComponent);
        }
    }
});
//# sourceMappingURL=slice2D.component.js.map