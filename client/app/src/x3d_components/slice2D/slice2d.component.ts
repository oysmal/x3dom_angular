import { Component, Output, EventEmitter, Input, ElementRef, ViewChild, ChangeDetectorRef, OnChanges, OnDestroy, AfterViewInit} from '@angular/core';
import { BoxShapeComponent } from '../boxShape/boxShape.component';
import { SubstituteGeometry } from '../substituteGeometry/substituteGeometry.component';
import { ModalService } from '../../modal/modal.service';
import { SliceService } from '../../slices/slice.service';
import { Slice } from '../../slices/slice';
declare var x3dom:any;

@Component({
    selector: '[slice2D]',
    template: `
        <transform>
            <transform [attr.render]="visible" boxShape [attr.translation]="slice.translation" [attr.rotation]="slice.rotation" (clicked)="onClick($event);" [box_textureUrl]="slice.textureUrl" [box_size]="slice.size" ></transform>
            <transform [attr.render]="!visible" substitute [attr.translation]="slice.translation" (clicked)="onClick($event);"></transform>
        </transform>
    `,
})
export class Slice2DComponent implements OnChanges, OnDestroy, AfterViewInit {
    @Input("slice") slice;
    visible:boolean = true;
    //@Output("clicked") clicked = new EventEmitter();

    constructor(private ref:ChangeDetectorRef, private el:ElementRef, private modalService:ModalService, private sliceService:SliceService) {
        this.sliceService.sliceChanged.subscribe(x => {
            console.log("Slice changed after init");
            if(this.slice && this.slice.id == x.id) {
                console.log("This.visible: " + this.visible);
                console.log("New visible: " + x.visible);
                this.slice.visible = x.visible;
                this.visible = x.visible;
                this.ref.detectChanges();
            }
        });
    }

    ngOnChanges() {
        console.log("changes in slice2d component");
    }

    ngOnDestroy() {
        console.log("destroy slice2d");
    }

    ngAfterViewInit() {
    }

    onClick(event) {
        //this.clicked.emit(this.slice);

        if(event.button == 1) {
            let slice = new Slice(this.slice);
            slice.visible = !slice.visible;
            this.visible = !this.visible;
            this.ref.detectChanges();
            console.log("slice changed");
            this.sliceService.changeSlice(slice);
        } else {
            this.modalService
                .reset()
                .setShowBasicControllers(false)
                .setSlice(this.slice)
                .show();
        }
    }
}