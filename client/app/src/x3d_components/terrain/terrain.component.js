System.register(['angular2/core', '../boxShape/boxShape.component', '../bvhRefiner/bvhRefinerSettings.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, boxShape_component_1, bvhRefinerSettings_service_1;
    var TerrainComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (boxShape_component_1_1) {
                boxShape_component_1 = boxShape_component_1_1;
            },
            function (bvhRefinerSettings_service_1_1) {
                bvhRefinerSettings_service_1 = bvhRefinerSettings_service_1_1;
            }],
        execute: function() {
            TerrainComponent = (function () {
                function TerrainComponent(elem, bvhRefinerSettings, cd) {
                    var _this = this;
                    this.cd = cd;
                    this.ready = false;
                    console.log("terrain created");
                    this.elem = elem;
                    this.bvhRefinerSettings = bvhRefinerSettings;
                    this.settings = this.bvhRefinerSettings.getSettings();
                    this.bvhRefinerSettings.getUpdateHandle().subscribe(function (settings) {
                        _this.settings = settings;
                        _this.cd.markForCheck();
                        _this.ready = true;
                        //x3dom.reload();
                    });
                }
                TerrainComponent.prototype.onload = function () {
                    console.log(this.settings);
                };
                TerrainComponent = __decorate([
                    core_1.Component({
                        selector: '[terrain]',
                        template: "\n        <BVHRefiner *ngIf=\"ready\"\n            [attr.maxDepth]='settings.maxDepth' \n            [attr.minDepth]='settings.minDepth' \n            [attr.interactionDepth]='settings.interactionDepth'\n            [attr.smoothLoading]='settings.smoothLoading' \n            [attr.subdivision]='settings.subdivision' \n            [attr.size]='settings.size' \n            [attr.factor]='settings.factor' \n            [attr.maxElevation]='settings.maxElevation' \n            [attr.elevationUrl]='settings.elevationUrl' \n            [attr.textureUrl]='settings.textureUrl' \n            [attr.normalUrl]='settings.normalUrl' \n            [attr.elevationFormat]='settings.elevationFormat' \n            [attr.textureFormat]='settings.textureFormat' \n            [attr.normalFormat]='settings.normalFormat' \n            [attr.mode]='settings.mode'\n            (load)=\"onload\">\n\t\t</BVHRefiner>\n\t",
                        directives: [boxShape_component_1.BoxShapeComponent]
                    }), 
                    __metadata('design:paramtypes', [core_1.ElementRef, bvhRefinerSettings_service_1.BvhRefinerSettingsService, core_1.ChangeDetectorRef])
                ], TerrainComponent);
                return TerrainComponent;
            }());
            exports_1("TerrainComponent", TerrainComponent);
        }
    }
});
//# sourceMappingURL=terrain.component.js.map