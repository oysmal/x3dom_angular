import { Component, ElementRef, Input, ChangeDetectorRef } from '@angular/core';
import {BoxShapeComponent} from '../boxShape/boxShape.component';
import {BvhRefinerSettingsService} from '../bvhRefiner/bvhRefinerSettings.service';

declare var x3dom: any;

@Component({
	selector: '[terrain]',
	template: `
        <transform rotation="-1.57075,0,0">
            <BVHRefiner *ngIf="ready"
                [attr.maxDepth]='settings.maxDepth' 
                [attr.minDepth]='settings.minDepth' 
                [attr.interactionDepth]='settings.interactionDepth'
                [attr.smoothLoading]='settings.smoothLoading' 
                [attr.subdivision]='settings.subdivision' 
                [attr.size]='settings.size' 
                [attr.factor]='settings.factor' 
                [attr.maxElevation]='settings.maxElevation' 
                [attr.elevationUrl]='settings.elevationUrl' 
                [attr.textureUrl]='settings.textureUrl' 
                [attr.normalUrl]='settings.normalUrl' 
                [attr.elevationFormat]='settings.elevationFormat' 
                [attr.textureFormat]='settings.textureFormat' 
                [attr.normalFormat]='settings.normalFormat' 
                [attr.mode]='settings.mode'
                [attr.solid]='false'
                (load)="onload">
            </BVHRefiner>
        </transform>
	`
	//directives: [BoxShapeComponent]
})
export class TerrainComponent {

    settings;
	elem;
    bvhRefinerSettings;
    ready = false;

	constructor(elem: ElementRef, bvhRefinerSettings: BvhRefinerSettingsService, private cd: ChangeDetectorRef) {
        //console.log("terrain created");
		this.elem = elem;
        this.bvhRefinerSettings = bvhRefinerSettings;
        this.settings = this.bvhRefinerSettings.getSettings();

        this.bvhRefinerSettings.getUpdateHandle().subscribe((settings) => {
            this.settings = settings;
            this.cd.markForCheck();
            this.ready = true;
            //x3dom.reload();
        });
	}

    onload() {
        //console.log(this.settings);
    }

}