import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { Slice } from '../../slices/slice';

@Injectable()
export class ViewpointService {


    activationEvent:ReplaySubject<string> = new ReplaySubject();

    triggerViewpoint(id:string):void {
        this.activationEvent.next(id);
    }

    getUpdateHandle():ReplaySubject<string> {
        return this.activationEvent;
    }
}
