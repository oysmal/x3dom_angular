import { Component, ViewChild, Input, AfterContentInit, OnDestroy } from '@angular/core';
import { Slice } from '../../slices/slice';
import { ViewpointService } from './viewpoint.service';
import { X3DOMUtilsService } from '../x3d/x3dom_utils.service';

declare var x3dom:any;

@Component({
    selector: '[viewpoint]',
    template: `
        <Viewpoint #view [attr.position]="position"></Viewpoint>
    `
})
export class ViewpointComponent implements AfterContentInit, OnDestroy{
    @ViewChild("view") view;
    @Input("slice") slice:Slice;
    position;
    orientation;
    distanceX = 30;
    
    constructor(private viewpointService:ViewpointService, private x3domUtilsService:X3DOMUtilsService) {}

    makeLookAt(eye, at, up) {
        let viewVector = at.subtract(eye).normalize();
        up = up.normalize();
        let s = viewVector.cross(up);
        let newUp = s.cross(viewVector);
        let lookAt = x3dom.fields.SFMatrix4f.identity();
        lookAt.setValue(s, newUp, viewVector.negate(), new x3dom.fields.SFVec3f(0.0, 0.0, 0.0));
        lookAt = lookAt.transpose();
        lookAt = lookAt.mult(x3dom.fields.SFMatrix4f.translation(eye.negate()));            
        console.log("LOOKAT");
        console.log(lookAt);
        return lookAt;
    }
    ngOnDestroy() {
        console.log("On destroy viewpoint");
    }

    ngAfterContentInit() {
        if(!this.slice) return;
        //TODO: Add code to get viewpoint position a small distance from the slice(perpendicular),
        // and orientation so the viewpoint looks perpendicularly at the slice.

        console.log("ngAfterContentInit");
        console.log("slice translation: " + this.slice.translation);
        // Calculate position of viewpoint
        let pos = this.slice.translation.split(',').map(x => parseInt(x));
        let a = this.slice.startPos.split(',').map(x => parseInt(x));
        let b = this.slice.endPos.split(',').map(x => parseInt(x));
        let negReciprocal:number = (b[0]-a[0]) == 0 ? 0 : -(b[1]-a[1])/(b[0]-a[0]); // take into account zero-division.
        let halfwayPoint = [(b[0]-a[0])/2+a[0], (b[1]-a[1])/2+a[1]];
        this.position = [halfwayPoint[0] + this.distanceX, halfwayPoint[1] + this.distanceX*negReciprocal, pos[2]].join(',');

        console.log("viewpoint position: " + this.position);
        // Calculate orientation of viewpoint
        let slicePos = x3dom.fields.SFVec3f.parse(pos.join(','));
        let viewpointPos = x3dom.fields.SFVec3f.parse(this.position);
        let up = x3dom.fields.SFVec3f.parse("0,0,1");
        //let fwd = x3dom.fields.SFVec3f.parse((pos[0]-this.position[0]) + "," + (pos[1]-this.position[1]) + ",0").normalize();
        //let right = fwd.cross(up);
        //up = right.cross(fwd);
        let viewMat = this.makeLookAt(viewpointPos, slicePos, up);//x3dom.fields.SFMatrix4f.lookAt(position3f, pos3f, up).inverse();
        console.log("VIEW");
        console.log(this.view);
        this.x3domUtilsService.load.subscribe(() => {
            setTimeout( () => this.view.nativeElement._x3domNode._viewMatrix = viewMat, 2000);
        });
        /*if(this.view.nativeElement._x3domNode) {
            this.view.nativeElement._x3domNode._viewMatrix = viewMat;
        } else {
            setTimeout( () => this.view.nativeElement._x3domNode._viewMatrix = viewMat, 5000);
        }*/
        
        // Activation code
        this.viewpointService.getUpdateHandle().subscribe( x => {
            if(this.slice.id == x) {
                console.log(this.slice.id);
                console.log(this.slice.translation);
                console.log(this.position);
                this.view.nativeElement.setAttribute('set_bind','true');
                /*let x3d:any;
                x3d = document.querySelector('x3d');
                x3d.runtime.examine();*/
            }
        });
    }
}