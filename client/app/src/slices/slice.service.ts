import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import { ApiConstants } from '../../../.tmp/apiConstants';
import { ReplaySubject, Subject } from 'rxjs';
import { Slice } from './slice';

declare var $:any;

@Injectable()
export class SliceService {

    private sliceResourceUrl = ApiConstants.api + "/slice_model";
    private _slices: Array<Slice> = new Array<Slice>();
    public slices: ReplaySubject<Slice[]> = new ReplaySubject();
    public sliceChanged: ReplaySubject<Slice> = new ReplaySubject();

    constructor(private http: Http) {
        this.loadSlices().subscribe( (data:any) => {
            data.map((x:any, i) => {
                x.id = x._id;
                x.textureUrl = ApiConstants.api + x.imageUrl;
                x.translation = x.position;
                x.index = i;
                let a = x.startPos.split(',').map(x => parseFloat(x));
                let b = x.endPos.split(',').map(x => parseFloat(x));
                let hyp = Math.sqrt(Math.pow(b[0] - a[0], 2) + Math.pow(b[1] - a[1], 2));
                let theta = Math.acos(Math.abs(b[0] - a[0]) / hyp);
                x.rotation = "0,0,1," + theta;
                x.size = hyp + ",0.1," + (x.end_depth - x.start_depth);
                x.visible = true;
                this._slices.push(new Slice(x));
            });

            this.slices.next(this._slices);
        })
    }

    loadSlices() {
        return this.http.get(this.sliceResourceUrl)
            .map((res: Response) => {
                let body = res.json();
                return body || {};
            }).catch((err: any) => {
                console.log(err);
                return err;
            });
    }

    changeSlice(slice:Slice) {
        this._slices[slice.index.toFixed()] = new Slice(slice);
        this.slices.next(this._slices);
        this.sliceChanged.next(slice);
    }

}