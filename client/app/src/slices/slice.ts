export class Slice {
    public id: string;
    public index: Number;
    public type: string;
    public name: string;
    public description: string;
    public start_n: Number;
    public start_e: Number;
    public startPos: string;
    public translation: string;
    public rotation: string;
    public size: string;
    public end_n: Number;
    public end_e: Number;
    public endPos: string;
    public start_depth: Number;
    public end_depth: Number;
    public textureUrl: any;
    public visible: boolean;
    public related_links: Array<string>;

    //constructor();
    constructor(obj?:any) {
        this.id = obj.id;
        this.index = obj.index;
        this.type = obj.type;
        this.name = obj.name;
        this.description = obj.description;
        this.start_n = obj.start_n;
        this.start_e = obj.start_e;
        this.startPos = obj.startPos;
        this.translation = obj.translation;
        this.rotation = obj.rotation;
        this.size = obj.size;
        this.end_n = obj.end_n;
        this.end_e = obj.end_e;
        this.endPos = obj.endPos;
        this.start_depth = obj.start_depth;
        this.end_depth = obj.end_depth;
        this.textureUrl = obj.textureUrl;
        this.visible = obj.visible;
        this.related_links = obj.related_links;
    }
}