import { Component } from '@angular/core';
import { SliceService } from './slice.service';
import { Slice } from './slice';
import { ViewpointService } from '../x3d_components/viewpoints/viewpoint.service';
import { ModalService } from '../modal/modal.service';

declare var $:any;

@Component({
    selector: 'slice-toggler',
    template: `
        <div class="settings-element">
            <div class="settings-element-header" (click)="toggle()">
                <h3>Toggle Slices
                    <i class="toggle-div fa" [class.fa-minus]="maximized" [class.fa-plus]="!maximized"></i>
                </h3>
            </div>
            <div class="settings-element-content" *ngIf="maximized">
                <div *ngFor="let slice of slices;" class="input-group">
                    <span class="input-group-addon">
                        <input type="checkbox" [checked]="slice.visible" (change)="onChange(slice)" />
                    </span>
                    <p>{{slice.name}}</p>
                    <span class="input-group-btn">
                        <button class="btn btn-primary" (click)="goToSliceViewpoint($event, slice)">View</button>
                    </span>
                    <span class="input-group-btn">
                        <button class="btn btn-info" (click)="showInfo($event, slice)">Info</button>
                    </span>
                </div>
            </div>
        </div>
    `,
    styles: [`
        :host {
            width: 100%;
        }
        .input-group {
            margin-bottom: 5px;
        }
        p {
            text-align: center;
            margin: 0;
            padding-left: 5px;
            padding-right: 5px;
            border: 1px solid #dddddd;
            border-radius: 2px;
            width: 100%;
        }

        .settings-element-content {
            border-bottom: 3px solid #aaaaaa;
        }
    `]
})
export class SliceControlComponent {
    slices:Slice[];
    maximized: boolean = false;
    previous_bound_viewpoint_id = null;

    constructor(private sliceService:SliceService, private modalService:ModalService) {
        this.sliceService.slices.subscribe(slices => {
            this.slices = slices;
        });
    }

    onChange(e) {
        e.visible = !e.visible;
        this.sliceService.changeSlice(e);
    }

    toggle() {
        this.maximized = !this.maximized;
    }

    showInfo(e, slice) {
        this.modalService
                .setShowBasicControllers(false)
                .setSlice(slice)
                .show();
    }

    goToSliceViewpoint(e, slice) {
        if(this.previous_bound_viewpoint_id) {
            $('#'+this.previous_bound_viewpoint_id).attr('set_bind', 'false');
        }
        this.previous_bound_viewpoint_id = slice.id + '_viewpoint';
        $('#'+this.previous_bound_viewpoint_id).attr('set_bind', 'true');
    }
}