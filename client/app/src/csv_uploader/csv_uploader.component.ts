import { Component } from '@angular/core';
import { SliceUploadService } from '../upload_slice/slice_upload.service';
import { CoordinateConverterService } from '../upload_slice/coordinate_converter/coordinate_converter.service';
import * as proj4 from 'proj4';

import * as Papa from 'papaparse';

@Component({
    selector: 'csv-uploader',
    templateUrl: './csv_uploader_form.template.html',
    styles: [`
        body {
            background-color: white;
        }
    `]
})
export class CSVUploaderComponent {
    csvdata;
    fileTarget;
    disabled = true;
    files;
    wsg84proj = 'EPSG:4326';
    utmProj = '+proj=utm +zone=33X +ellps=GRS80 +units=m +no_defs';

    constructor(private coordinateConverterService: CoordinateConverterService, private sliceUploadService:SliceUploadService) {
    }

    upload() {
        this.readFile(this.fileTarget);
    }

    fileChange($event) {
        this.fileTarget = $event.target;
        this.disabled = false;
    }

    folderChange($event) {
        this.files = $event.target.files;
    }

    readFile(inputValue: any): void {
        let file: File = inputValue.files[0];

        let config = {
            delimiter: "",	// auto-detect
            newline: "",	// auto-detect
            header: true,
            dynamicTyping: false,
            preview: 0,
            encoding: "",
            worker: false,
            comments: false,
            step: undefined,
            complete: (result, file) => {
                console.log("fin");
                console.log(result);
                this.createProperSliceDataObjectsFromResult(result.data);
            },
            error: (err, file) => {
                console.log("err");
                console.log(err);
            },
            download: false,
            skipEmptyLines: false,
            chunk: undefined,
            fastMode: undefined,
            beforeFirstChunk: undefined,
            withCredentials: undefined
        }

        Papa.parse(file, config);
    }

    createProperSliceDataObjectsFromResult(data:any) {

        // loop through each element of "data"
        data.map((x) => {
            if(!x.additional_files) return;
            // Create array containing names of the png files related to the current element of data
            let relevant_png_filename = x.additional_files.split(',').filter( a => {
                return (/^.+.png$/).exec(a);
            })[0];

            let png_file = null;

            // loop through all files that were selected for potential upload
            for(let i = 0; i < this.files.length; i++) {
                // if the current file's name == relevant_png_filename,
                // add the file for upload
                if(this.files[i].name == relevant_png_filename) {
                    png_file = this.files[i];
                }
            }

            let longlatStart = [parseFloat(x.start_e.split(',').join('.')), parseFloat(x.start_n.split(',').join('.'))];
            let longlatEnd = [parseFloat(x.end_e.split(',').join('.')), parseFloat(x.end_n.split(',').join('.'))];

            let utmStart33X = proj4(this.wsg84proj, this.utmProj).forward(longlatStart);
            let startPos = this.coordinateConverterService
                .getPositionInLocalCoordinateSystem(utmStart33X[0], utmStart33X[1]);

            let utmEnd33X = proj4(this.wsg84proj, this.utmProj).forward(longlatEnd);
            let endPos = this.coordinateConverterService
                .getPositionInLocalCoordinateSystem(utmEnd33X[0], utmEnd33X[1]);
            
            let startDepth = this.coordinateConverterService.getDepthInLocalCoordinateSystem(parseFloat(x.start_depth));
            let endDepth = this.coordinateConverterService.getDepthInLocalCoordinateSystem(parseFloat(x.end_depth));

            // upload the current data, along with PNG file
            let upload_content = {
                type: 'image/png',
                name: x.dataset_name,
                description: x.info,
                start_n: parseFloat(x.start_n.split(',').join('.')),
                start_e: parseFloat(x.start_e.split(',').join('.')),
                startPos: startPos.x + "," + startPos.y + "," + (startDepth-endDepth)/2,
                position: (startPos.x+endPos.x)/2 + "," + (startPos.y+endPos.y)/2 + "," + (startDepth-endDepth)/2,
                end_n: parseFloat(x.end_n.split(',').join('.')),
                end_e: parseFloat(x.end_e.split(',').join('.')),
                endPos: endPos.x + "," + endPos.y + "," + (startDepth-endDepth)/2,
                start_depth: startDepth,
                end_depth: endDepth,
                data: png_file,
                related_links: x.related_links.split(',')
            }

            this.sliceUploadService.create_request(upload_content).subscribe(x => {
                console.log(x); 
            });
        });
    }
}