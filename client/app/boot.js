System.register(['angular2/platform/browser', './src/app.component', './src/x3d_components/bvhRefiner/bvhRefinerSettings.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var browser_1, app_component_1, bvhRefinerSettings_service_1;
    return {
        setters:[
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (app_component_1_1) {
                app_component_1 = app_component_1_1;
            },
            function (bvhRefinerSettings_service_1_1) {
                bvhRefinerSettings_service_1 = bvhRefinerSettings_service_1_1;
            }],
        execute: function() {
            browser_1.bootstrap(app_component_1.AppComponent, [bvhRefinerSettings_service_1.BvhRefinerSettingsService]);
        }
    }
});
//# sourceMappingURL=boot.js.map