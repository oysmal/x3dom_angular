
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {AppModule} from './src/app.module';
import {BvhRefinerSettingsService} from './src/x3d_components/bvhRefiner/bvhRefinerSettings.service';
// Add all operators to Observable
import 'rxjs/Rx';
import { enableProdMode } from '@angular/core';

/*if (process.env.ENV === 'production') {
  enableProdMode();
}
*/
platformBrowserDynamic().bootstrapModule(AppModule);