const webpackMerge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const commonConfig = require('./webpack.common.js');
const helpers = require('./helpers');
const path = require('path');

module.exports = webpackMerge(commonConfig, {
    devtool: 'cheap-module-eval-source-map',

    output: {
        path: helpers.root('dist'),
        filename: '[name].js',
        chunkFilename: '[id].chunk.js'
    },

    plugins: [
        new CleanWebpackPlugin(['dist'], {
            root: path.join(__dirname, '../'),
            verbose: true,
            dry: false,
            exclude: []
        }),
        new ExtractTextPlugin('[name].css'),
        new CopyWebpackPlugin([
            { from: 'client/app/resources', to: 'resources' },
            { from: 'client/app/styles', to: 'styles' },
            { from: 'client/x3dom_full.js', to: 'x3dom_full.js' },
            { from: 'node_modules/bootstrap/dist/css', to: 'styles/bootstrap' },
            { from: 'node_modules/bootstrap/dist/js', to: 'styles/bootstrap' },
            { from: 'client/index.html', to: '' }
        ])
    ],

    devServer: {
        historyApiFallback: true,
        stats: 'minimal'
    }
});