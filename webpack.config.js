const dev = require('./webpack/webpack.dev.js');
const prod = require('./webpack/webpack.production.js');

let config = dev;

switch(process.env.npm_lifcycle_event) {
    case 'build':
        config = prod;
        break;
    default: 
        config = dev;
        break;
}

module.exports = config;