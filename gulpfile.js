const gulp = require('gulp');
const gutil = require('gutil');
const fs = require('fs');
const gulpIf = require('gulp-if');
const webpack = require('webpack-stream');
const zip = require('gulp-zip');
const clean = require('gulp-clean');
const minimist = require('minimist');

const paths = {
    src: {
        archive: 'dist/**/*',
        styles: 'client/app/styles/*.css',
        resources: 'client/app/resources/**/*',
        jsEntry: 'client/app/boot.ts',
        allsrc: 'client/**/*',
        x3dom: 'client/x3dom_full.js',
        bootstrap: {
            css: 'node_modules/bootstrap/dist/css/bootstrap.min.css',
            js: 'node_modules/bootstrap/dist/js/bootstrap.min.js',
            fonts: 'node_modules/bootstrap/dist/fonts/*'
        }
    },
    dest: {
        root: 'dist/',
        styles: 'dist/styles/',
        bootstrap: 'dist/styles/bootstrap/',
        resources: 'dist/resources/',
    }
}

gulp.task("copy:bootstrap", ['clean'], () => {
    return gulp.src([
        paths.src.bootstrap.css, 
        paths.src.bootstrap.js, 
        paths.src.bootstrap.fonts])
        .pipe(gulp.dest(paths.dest.bootstrap));
});

gulp.task("copy:styles", ['clean'], () => {
    return gulp.src(paths.src.styles)
        .pipe(gulp.dest(paths.dest.styles));
});

gulp.task("copy:resources", ['clean'], () => {
    return gulp.src(paths.src.resources)
        .pipe(gulp.dest(paths.dest.resources));
});

gulp.task("copy:x3dom", ['clean'], () => {
    return gulp.src(paths.src.x3dom)
        .pipe(gulp.dest(paths.dest.root));
});

gulp.task("copy", 
    ['clean', 'copy:styles', 'copy:bootstrap', 'copy:resources', 'copy:x3dom']);

gulp.task("webpack:dev", ['copy', 'readApiUrl:dev'], () => {
    return gulp.src(paths.src.jsEntry)
        .pipe(webpack(require('./webpack/webpack.dev.js')))
        .pipe(gulp.dest(paths.dest.root));
});

gulp.task("watch", ()=> {
    return gulp.watch(paths.src.allsrc, ["webpack:dev"]);
});

gulp.task("webpack:build", ['copy', 'readApiUrl'], (done) => {
    return gulp.src(paths.src.jsEntry)
        .pipe(webpack(require('./webpack/webpack.production.js')))
        .pipe(gulp.dest(paths.dest.root)).on('end', () => {
            onBuild(done);
        });
});

function onBuild(done) {
    return function (err, stats) {
        if (err) {
            gutil.log('Error', err);
            if (done) {
                done();
            }
        } else {
            Object.keys(stats.compilation.assets).forEach(function (key) {
                gutil.log('Webpack: output ', gutil.colors.green(key));
            });
            gutil.log('Webpack: ', gutil.colors.blue('finished ', stats.compilation.name));
            if (done) {
                done();
            }
        }
    }
}

gulp.task("archive", ['build'], () => {
    return gulp.src(paths.src.archive)
        .pipe(zip('geovis.zip'))
        .pipe(gulp.dest(paths.dest.root));
});

//gulp.task('watch', () => {
//    let tswatcher = gulp.watch('client/**/*.{ts,html}', ['webpack:dev']);
//    tswatcher.on('change', function (event) {
//        console.log('File ' + event.path + ' was ' + event.type);
//    });
//
//    let otherwatcher = gulp.watch('client/**/*', ['copy']);
//    otherwatcher.on('change', function (event) {
//        console.log('File ' + event.path + ' was ' + event.type);
//    });
//});

gulp.task('clean', () => {
    return gulp.src(paths.dest.root, { read: false })
        .pipe(clean());

});

gulp.task('readApiUrl', () => {

    let knownOptions = {
        string: 'api',
        string: 'base',
        default: { api: '', base: '' }
    };

    let options = minimist(process.argv.slice(2), knownOptions);
    const ApiModuleText = 'export const ApiConstants={"api":"' + options.api + '"}';
    console.log(options);
    return fs.writeFileSync("client/.tmp/apiConstants.ts", ApiModuleText);
});

gulp.task('readApiUrl:dev', () => {
    const ApiModuleText = 'export const ApiConstants={"api":"http://localhost:8000/api"}';
    console.log(ApiModuleText);
    return fs.writeFileSync("client/.tmp/apiConstants.ts", ApiModuleText);
});

gulp.task('default', ['copy', 'webpack:dev']);

gulp.task('build', ['webpack:build']);
